<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="mainwindow.cpp" line="102"/>
        <location filename="ui_mainwindow.h" line="1007"/>
        <source>Package Installer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="57"/>
        <location filename="ui_mainwindow.h" line="1016"/>
        <source>Popular Applications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="73"/>
        <location filename="ui_mainwindow.h" line="1012"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="83"/>
        <location filename="ui_mainwindow.h" line="1011"/>
        <source>Package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="88"/>
        <location filename="ui_mainwindow.h" line="1010"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="93"/>
        <location filename="mainwindow.ui" line="468"/>
        <location filename="mainwindow.ui" line="660"/>
        <location filename="mainwindow.ui" line="1030"/>
        <location filename="ui_mainwindow.h" line="1009"/>
        <location filename="ui_mainwindow.h" line="1041"/>
        <location filename="ui_mainwindow.h" line="1055"/>
        <location filename="ui_mainwindow.h" line="1083"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="101"/>
        <location filename="ui_mainwindow.h" line="1013"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Manage popular packages&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="120"/>
        <location filename="mainwindow.ui" line="191"/>
        <location filename="mainwindow.ui" line="730"/>
        <location filename="mainwindow.ui" line="1055"/>
        <location filename="mainwindow.ui" line="1274"/>
        <location filename="ui_mainwindow.h" line="1014"/>
        <location filename="ui_mainwindow.h" line="1017"/>
        <location filename="ui_mainwindow.h" line="1067"/>
        <location filename="ui_mainwindow.h" line="1086"/>
        <location filename="ui_mainwindow.h" line="1116"/>
        <source>search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="149"/>
        <location filename="mainwindow.ui" line="204"/>
        <location filename="mainwindow.ui" line="772"/>
        <location filename="mainwindow.ui" line="1118"/>
        <location filename="mainwindow.ui" line="1245"/>
        <location filename="ui_mainwindow.h" line="1015"/>
        <location filename="ui_mainwindow.h" line="1018"/>
        <location filename="ui_mainwindow.h" line="1069"/>
        <location filename="ui_mainwindow.h" line="1096"/>
        <location filename="ui_mainwindow.h" line="1115"/>
        <source>= Installed packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <location filename="ui_mainwindow.h" line="1044"/>
        <source>Enabled Repos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="237"/>
        <location filename="ui_mainwindow.h" line="1020"/>
        <source>Remove orphan packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="264"/>
        <location filename="mainwindow.ui" line="1337"/>
        <location filename="ui_mainwindow.h" line="1021"/>
        <location filename="ui_mainwindow.h" line="1121"/>
        <source>Upgrade All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="278"/>
        <location filename="mainwindow.ui" line="558"/>
        <location filename="mainwindow.ui" line="886"/>
        <location filename="ui_mainwindow.h" line="1022"/>
        <location filename="ui_mainwindow.h" line="1049"/>
        <location filename="ui_mainwindow.h" line="1074"/>
        <source>Installed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="301"/>
        <location filename="mainwindow.ui" line="508"/>
        <location filename="mainwindow.ui" line="907"/>
        <location filename="ui_mainwindow.h" line="1023"/>
        <location filename="ui_mainwindow.h" line="1045"/>
        <location filename="ui_mainwindow.h" line="1077"/>
        <source>Total packages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <location filename="mainwindow.ui" line="615"/>
        <location filename="mainwindow.ui" line="947"/>
        <location filename="ui_mainwindow.h" line="1025"/>
        <location filename="ui_mainwindow.h" line="1053"/>
        <location filename="ui_mainwindow.h" line="1080"/>
        <source>Also Install &quot;Recommended&quot; Packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="322"/>
        <location filename="mainwindow.ui" line="588"/>
        <location filename="mainwindow.ui" line="879"/>
        <location filename="ui_mainwindow.h" line="1026"/>
        <location filename="ui_mainwindow.h" line="1051"/>
        <location filename="ui_mainwindow.h" line="1073"/>
        <source>Upgradable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <location filename="mainwindow.ui" line="522"/>
        <location filename="mainwindow.ui" line="914"/>
        <location filename="ui_mainwindow.h" line="1027"/>
        <location filename="ui_mainwindow.h" line="1047"/>
        <location filename="ui_mainwindow.h" line="1078"/>
        <source>Hide library and developer packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="351"/>
        <location filename="mainwindow.ui" line="544"/>
        <location filename="mainwindow.ui" line="966"/>
        <location filename="ui_mainwindow.h" line="1028"/>
        <location filename="ui_mainwindow.h" line="1048"/>
        <location filename="ui_mainwindow.h" line="1081"/>
        <source>Refresh list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="390"/>
        <location filename="mainwindow.ui" line="688"/>
        <location filename="mainwindow.ui" line="1085"/>
        <location filename="mainwindow.ui" line="1197"/>
        <location filename="ui_mainwindow.h" line="1036"/>
        <location filename="ui_mainwindow.h" line="1064"/>
        <location filename="ui_mainwindow.h" line="1093"/>
        <location filename="ui_mainwindow.h" line="1112"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filter packages according to their status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="393"/>
        <location filename="mainwindow.ui" line="397"/>
        <location filename="mainwindow.ui" line="691"/>
        <location filename="mainwindow.ui" line="695"/>
        <location filename="mainwindow.ui" line="1088"/>
        <location filename="mainwindow.ui" line="1092"/>
        <location filename="mainwindow.cpp" line="2543"/>
        <location filename="ui_mainwindow.h" line="1030"/>
        <location filename="ui_mainwindow.h" line="1038"/>
        <location filename="ui_mainwindow.h" line="1058"/>
        <location filename="ui_mainwindow.h" line="1066"/>
        <location filename="ui_mainwindow.h" line="1087"/>
        <location filename="ui_mainwindow.h" line="1095"/>
        <source>All packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="402"/>
        <location filename="mainwindow.ui" line="700"/>
        <location filename="mainwindow.ui" line="1097"/>
        <location filename="mainwindow.cpp" line="2556"/>
        <location filename="ui_mainwindow.h" line="1031"/>
        <location filename="ui_mainwindow.h" line="1059"/>
        <location filename="ui_mainwindow.h" line="1088"/>
        <source>Installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="407"/>
        <location filename="mainwindow.ui" line="705"/>
        <location filename="mainwindow.ui" line="1102"/>
        <location filename="mainwindow.cpp" line="2554"/>
        <location filename="ui_mainwindow.h" line="1032"/>
        <location filename="ui_mainwindow.h" line="1060"/>
        <location filename="ui_mainwindow.h" line="1089"/>
        <source>Upgradable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="412"/>
        <location filename="mainwindow.ui" line="710"/>
        <location filename="mainwindow.ui" line="1107"/>
        <location filename="mainwindow.ui" line="1234"/>
        <location filename="mainwindow.cpp" line="2528"/>
        <location filename="mainwindow.cpp" line="2559"/>
        <location filename="mainwindow.cpp" line="2651"/>
        <location filename="mainwindow.cpp" line="2652"/>
        <location filename="ui_mainwindow.h" line="1033"/>
        <location filename="ui_mainwindow.h" line="1061"/>
        <location filename="ui_mainwindow.h" line="1090"/>
        <location filename="ui_mainwindow.h" line="1109"/>
        <source>Not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="432"/>
        <location filename="mainwindow.ui" line="752"/>
        <location filename="mainwindow.ui" line="814"/>
        <location filename="ui_mainwindow.h" line="1039"/>
        <location filename="ui_mainwindow.h" line="1068"/>
        <location filename="ui_mainwindow.h" line="1071"/>
        <source>= Upgradable package. Newer version available in selected repository.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="458"/>
        <location filename="mainwindow.ui" line="650"/>
        <location filename="mainwindow.ui" line="1020"/>
        <location filename="ui_mainwindow.h" line="1043"/>
        <location filename="ui_mainwindow.h" line="1057"/>
        <location filename="ui_mainwindow.h" line="1085"/>
        <source>Package Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="463"/>
        <location filename="mainwindow.ui" line="655"/>
        <location filename="mainwindow.ui" line="1025"/>
        <location filename="mainwindow.ui" line="1469"/>
        <location filename="ui_mainwindow.h" line="1042"/>
        <location filename="ui_mainwindow.h" line="1056"/>
        <location filename="ui_mainwindow.h" line="1084"/>
        <location filename="ui_mainwindow.h" line="1128"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="493"/>
        <location filename="ui_mainwindow.h" line="1070"/>
        <source>MX Test Repo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="796"/>
        <location filename="ui_mainwindow.h" line="1097"/>
        <source>Debian Backports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1142"/>
        <location filename="ui_mainwindow.h" line="1131"/>
        <source>Flatpaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1148"/>
        <location filename="mainwindow.ui" line="1152"/>
        <location filename="ui_mainwindow.h" line="1098"/>
        <location filename="ui_mainwindow.h" line="1101"/>
        <source>For all users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1157"/>
        <location filename="ui_mainwindow.h" line="1099"/>
        <source>For current user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1165"/>
        <location filename="ui_mainwindow.h" line="1102"/>
        <source>Remote (repo):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1200"/>
        <location filename="mainwindow.ui" line="1204"/>
        <location filename="mainwindow.cpp" line="2510"/>
        <location filename="ui_mainwindow.h" line="1103"/>
        <location filename="ui_mainwindow.h" line="1114"/>
        <source>All apps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1209"/>
        <location filename="mainwindow.cpp" line="2514"/>
        <location filename="ui_mainwindow.h" line="1104"/>
        <source>All runtimes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1214"/>
        <location filename="mainwindow.cpp" line="2518"/>
        <location filename="ui_mainwindow.h" line="1105"/>
        <source>All available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1219"/>
        <location filename="mainwindow.cpp" line="2508"/>
        <location filename="ui_mainwindow.h" line="1106"/>
        <source>Installed apps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1224"/>
        <location filename="mainwindow.cpp" line="2506"/>
        <location filename="ui_mainwindow.h" line="1107"/>
        <source>Installed runtimes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1229"/>
        <location filename="mainwindow.cpp" line="2526"/>
        <location filename="mainwindow.cpp" line="2645"/>
        <location filename="mainwindow.cpp" line="2646"/>
        <location filename="ui_mainwindow.h" line="1108"/>
        <source>All installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1316"/>
        <location filename="ui_mainwindow.h" line="1118"/>
        <source>Total items </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1330"/>
        <location filename="ui_mainwindow.h" line="1120"/>
        <source>Installed apps:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1383"/>
        <location filename="ui_mainwindow.h" line="1122"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1413"/>
        <location filename="ui_mainwindow.h" line="1123"/>
        <source>Total installed size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1427"/>
        <location filename="ui_mainwindow.h" line="1125"/>
        <source>Remove unused runtimes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1459"/>
        <location filename="ui_mainwindow.h" line="1130"/>
        <source>Short Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1464"/>
        <location filename="ui_mainwindow.h" line="1129"/>
        <source>Full Package Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1474"/>
        <location filename="ui_mainwindow.h" line="1127"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1504"/>
        <location filename="mainwindow.cpp" line="2317"/>
        <location filename="mainwindow.cpp" line="2466"/>
        <location filename="ui_mainwindow.h" line="1134"/>
        <source>Console Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1510"/>
        <location filename="ui_mainwindow.h" line="1132"/>
        <source>Enter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1521"/>
        <location filename="ui_mainwindow.h" line="1133"/>
        <source>Respond here, or just press Enter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1577"/>
        <location filename="mainwindow.cpp" line="1077"/>
        <location filename="mainwindow.cpp" line="2638"/>
        <location filename="mainwindow.cpp" line="2642"/>
        <location filename="mainwindow.cpp" line="2897"/>
        <location filename="ui_mainwindow.h" line="1135"/>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1584"/>
        <location filename="ui_mainwindow.h" line="1137"/>
        <source>Alt+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1603"/>
        <location filename="ui_mainwindow.h" line="1140"/>
        <source>Quit application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1606"/>
        <location filename="ui_mainwindow.h" line="1142"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1613"/>
        <location filename="ui_mainwindow.h" line="1144"/>
        <source>Alt+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1645"/>
        <location filename="ui_mainwindow.h" line="1147"/>
        <source>About this application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1648"/>
        <location filename="ui_mainwindow.h" line="1149"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1655"/>
        <location filename="ui_mainwindow.h" line="1151"/>
        <source>Alt+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1693"/>
        <location filename="ui_mainwindow.h" line="1155"/>
        <source>Display help </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1696"/>
        <location filename="ui_mainwindow.h" line="1157"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1703"/>
        <location filename="ui_mainwindow.h" line="1159"/>
        <source>Alt+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1719"/>
        <location filename="ui_mainwindow.h" line="1161"/>
        <source>Uninstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1726"/>
        <location filename="ui_mainwindow.h" line="1163"/>
        <source>Alt+U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="93"/>
        <source>Flatpak tab is disabled on 32-bit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="186"/>
        <source>Uninstalling packages...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="191"/>
        <source>Running pre-uninstall operations...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="206"/>
        <source>Running post-uninstall operations...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="220"/>
        <source>Refreshing sources...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="234"/>
        <location filename="mainwindow.cpp" line="1123"/>
        <location filename="mainwindow.cpp" line="1252"/>
        <location filename="mainwindow.cpp" line="1394"/>
        <location filename="mainwindow.cpp" line="1456"/>
        <location filename="mainwindow.cpp" line="2134"/>
        <location filename="mainwindow.cpp" line="2164"/>
        <location filename="mainwindow.cpp" line="2176"/>
        <location filename="mainwindow.cpp" line="2290"/>
        <location filename="mainwindow.cpp" line="2309"/>
        <location filename="mainwindow.cpp" line="2366"/>
        <location filename="mainwindow.cpp" line="2383"/>
        <location filename="mainwindow.cpp" line="2400"/>
        <location filename="mainwindow.cpp" line="2721"/>
        <location filename="mainwindow.cpp" line="2797"/>
        <location filename="mainwindow.cpp" line="2827"/>
        <location filename="mainwindow.cpp" line="2920"/>
        <location filename="mainwindow.cpp" line="2944"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="235"/>
        <source>There was a problem updating sources. Some sources may not have provided updates. For more info check: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="627"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="634"/>
        <source>Please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="816"/>
        <location filename="mainwindow.cpp" line="833"/>
        <source>Version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="816"/>
        <source> in stable repo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="818"/>
        <source>Not available in stable repo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="827"/>
        <location filename="mainwindow.cpp" line="2443"/>
        <source>Latest version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="827"/>
        <location filename="mainwindow.cpp" line="2443"/>
        <source> already installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="833"/>
        <source> installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="945"/>
        <source>You are about to use the MX Test repository, whose packages are provided for testing purposes only. It is possible that they might break your system, so it is suggested that you back up your system and install or update only one package at a time. Please provide feedback in the Forum so the package can be evaluated before moving up to Main.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="954"/>
        <source>You are about to use Debian Backports, which contains packages taken from the next Debian release (called &apos;testing&apos;), adjusted and recompiled for usage on Debian stable. They cannot be tested as extensively as in the stable releases of Debian and antiX Linux, and are provided on an as-is basis, with risk of incompatibilities with other components in Debian stable. Use with care!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="962"/>
        <source>MX Linux includes this repository of flatpaks for the users&apos; convenience only, and is not responsible for the functionality of the individual flatpaks themselves. For more, consult flatpaks in the Wiki.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="969"/>
        <location filename="mainwindow.cpp" line="2930"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="973"/>
        <source>Do not show this message again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1075"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1089"/>
        <source>The following packages were selected. Click Show Details for list of changes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1124"/>
        <location filename="mainwindow.cpp" line="1253"/>
        <location filename="mainwindow.cpp" line="1457"/>
        <source>Internet is not available, won&apos;t be able to download the list of packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1129"/>
        <source>Installing packages...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1186"/>
        <source>Post-processing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1216"/>
        <source>Pre-processing for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1229"/>
        <source>Installing </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1236"/>
        <source>Post-processing for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1395"/>
        <source>There was an error writing file: %1. Please check if you have enough free space on your drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1465"/>
        <source>Downloading package info...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1921"/>
        <location filename="mainwindow.cpp" line="1998"/>
        <location filename="mainwindow.cpp" line="2022"/>
        <source>Package info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1935"/>
        <location filename="mainwindow.cpp" line="2863"/>
        <source>More &amp;info...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1964"/>
        <source>Packages to be installed: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2130"/>
        <location filename="mainwindow.cpp" line="2143"/>
        <location filename="mainwindow.cpp" line="2158"/>
        <location filename="mainwindow.cpp" line="2173"/>
        <location filename="mainwindow.cpp" line="2262"/>
        <location filename="mainwindow.cpp" line="2285"/>
        <location filename="mainwindow.cpp" line="2717"/>
        <location filename="mainwindow.cpp" line="2791"/>
        <location filename="mainwindow.cpp" line="2821"/>
        <location filename="mainwindow.cpp" line="2914"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2130"/>
        <location filename="mainwindow.cpp" line="2143"/>
        <location filename="mainwindow.cpp" line="2158"/>
        <location filename="mainwindow.cpp" line="2173"/>
        <location filename="mainwindow.cpp" line="2262"/>
        <location filename="mainwindow.cpp" line="2285"/>
        <location filename="mainwindow.cpp" line="2303"/>
        <location filename="mainwindow.cpp" line="2717"/>
        <location filename="mainwindow.cpp" line="2791"/>
        <location filename="mainwindow.cpp" line="2821"/>
        <location filename="mainwindow.cpp" line="2914"/>
        <location filename="mainwindow.cpp" line="2938"/>
        <source>Processing finished successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2135"/>
        <location filename="mainwindow.cpp" line="2165"/>
        <location filename="mainwindow.cpp" line="2177"/>
        <location filename="mainwindow.cpp" line="2722"/>
        <location filename="mainwindow.cpp" line="2798"/>
        <location filename="mainwindow.cpp" line="2828"/>
        <source>Problem detected while installing, please inspect the console output.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2187"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2188"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2190"/>
        <source>Package Installer for antiX Linux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2192"/>
        <source>Copyright (c) MX Linux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2193"/>
        <source>%1 License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2206"/>
        <source>%1 Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2290"/>
        <source>We encountered a problem uninstalling, please check output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2303"/>
        <location filename="mainwindow.cpp" line="2938"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2309"/>
        <location filename="mainwindow.cpp" line="2944"/>
        <source>We encountered a problem uninstalling the program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2367"/>
        <location filename="mainwindow.cpp" line="2384"/>
        <location filename="mainwindow.cpp" line="2401"/>
        <source>Could not download the list of packages. Please check your APT sources.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2417"/>
        <location filename="mainwindow.cpp" line="2429"/>
        <source>Flatpak not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2418"/>
        <source>Flatpak is not currently installed.
OK to go ahead and install it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2429"/>
        <source>Flatpak was not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2453"/>
        <location filename="mainwindow.cpp" line="2475"/>
        <source>Flathub remote failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2453"/>
        <location filename="mainwindow.cpp" line="2475"/>
        <source>Flathub remote could not be added</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2461"/>
        <source>Needs re-login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2462"/>
        <source>You might need to logout/login to see installed items in the menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2638"/>
        <source>Upgrade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2750"/>
        <source>Quit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2751"/>
        <source>Process still running, quitting might leave the system in an unstable state.&lt;p&gt;&lt;b&gt;Are you sure you want to exit Package Installer?&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2895"/>
        <source>Reinstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2921"/>
        <source>Problem detected during last operation, please inspect the console output.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2931"/>
        <source>Potentially dangerous operation.
Please make sure you check carefully the list of packages to be removed.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManageRemotes</name>
    <message>
        <location filename="remotes.cpp" line="13"/>
        <source>Manage Flatpak Remotes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="22"/>
        <source>For all users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="23"/>
        <source>For current user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="29"/>
        <source>enter Flatpak remote URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="32"/>
        <source>enter Flatpakref location to install app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="34"/>
        <source>Add or remove flatpak remotes (repos), or install apps using flatpakref URL or path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="43"/>
        <source>Remove remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="48"/>
        <source>Add remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="53"/>
        <source>Install app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="58"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="75"/>
        <source>Not removable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="76"/>
        <source>Flathub is the main Flatpak remote and won&apos;t be removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="96"/>
        <source>Error adding remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="97"/>
        <source>Could not add remote - command returned an error. Please double-check the remote address and try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="102"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="remotes.cpp" line="102"/>
        <source>Remote added successfully</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.cpp" line="53"/>
        <location filename="about.cpp" line="62"/>
        <source>Changelog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.cpp" line="54"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>Package Installer is a tool used for managing packages on antiX Linux
    - installs popular programs from different sources
    - installs programs from Debian Backports repo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="80"/>
        <source>Skip online check if it falsely reports lack of internet access.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="86"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="87"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="97"/>
        <source>Unable to get exclusive lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.cpp" line="98"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
