<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <location filename="../mainwindow.cpp" line="100"/>
        <source>MX Package Installer</source>
        <translation>MX csomagtelepítő</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="57"/>
        <source>Popular Applications</source>
        <translation>Népszerű alkalmazások</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="73"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="83"/>
        <source>Package</source>
        <translation>Csomag</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="88"/>
        <source>Info</source>
        <translation>Infó</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="93"/>
        <location filename="../mainwindow.ui" line="468"/>
        <location filename="../mainwindow.ui" line="660"/>
        <location filename="../mainwindow.ui" line="1030"/>
        <source>Description</source>
        <translation>Leírás</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="101"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Manage popular packages&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="120"/>
        <location filename="../mainwindow.ui" line="191"/>
        <location filename="../mainwindow.ui" line="730"/>
        <location filename="../mainwindow.ui" line="1055"/>
        <location filename="../mainwindow.ui" line="1274"/>
        <source>search</source>
        <translation>keresés</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="149"/>
        <location filename="../mainwindow.ui" line="204"/>
        <location filename="../mainwindow.ui" line="772"/>
        <location filename="../mainwindow.ui" line="1118"/>
        <location filename="../mainwindow.ui" line="1245"/>
        <source>= Installed packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="173"/>
        <source>Enabled Repos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="237"/>
        <source>Remove orphan packages</source>
        <translation>Elárvult csomagok eltávolítása</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="264"/>
        <location filename="../mainwindow.ui" line="1337"/>
        <source>Upgrade All</source>
        <translation>Összes frissítése</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="278"/>
        <location filename="../mainwindow.ui" line="558"/>
        <location filename="../mainwindow.ui" line="886"/>
        <source>Installed:</source>
        <translation>Telepítve:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="301"/>
        <location filename="../mainwindow.ui" line="508"/>
        <location filename="../mainwindow.ui" line="907"/>
        <source>Total packages:</source>
        <translation>Összes csomag:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="315"/>
        <location filename="../mainwindow.ui" line="615"/>
        <location filename="../mainwindow.ui" line="947"/>
        <source>Also Install &quot;Recommended&quot; Packages</source>
        <translation>A &quot;Javasolt&quot; csomagokat is telepítse</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="322"/>
        <location filename="../mainwindow.ui" line="588"/>
        <location filename="../mainwindow.ui" line="879"/>
        <source>Upgradable:</source>
        <translation>Frissítendő:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="329"/>
        <location filename="../mainwindow.ui" line="522"/>
        <location filename="../mainwindow.ui" line="914"/>
        <source>Hide library and developer packages</source>
        <translation>Könyvtár és fejlesztői csomagok elrejtése</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="351"/>
        <location filename="../mainwindow.ui" line="544"/>
        <location filename="../mainwindow.ui" line="966"/>
        <source>Refresh list</source>
        <translation>Lista frissítése</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="390"/>
        <location filename="../mainwindow.ui" line="688"/>
        <location filename="../mainwindow.ui" line="1085"/>
        <location filename="../mainwindow.ui" line="1197"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filter packages according to their status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Csomagok szűrése állapotuk szerint.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="393"/>
        <location filename="../mainwindow.ui" line="397"/>
        <location filename="../mainwindow.ui" line="691"/>
        <location filename="../mainwindow.ui" line="695"/>
        <location filename="../mainwindow.ui" line="1088"/>
        <location filename="../mainwindow.ui" line="1092"/>
        <location filename="../mainwindow.cpp" line="2541"/>
        <source>All packages</source>
        <translation>Minden csomag</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="402"/>
        <location filename="../mainwindow.ui" line="700"/>
        <location filename="../mainwindow.ui" line="1097"/>
        <location filename="../mainwindow.cpp" line="2554"/>
        <source>Installed</source>
        <translation>Telepítve</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <location filename="../mainwindow.ui" line="705"/>
        <location filename="../mainwindow.ui" line="1102"/>
        <location filename="../mainwindow.cpp" line="2552"/>
        <source>Upgradable</source>
        <translation>Frissíthető</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="412"/>
        <location filename="../mainwindow.ui" line="710"/>
        <location filename="../mainwindow.ui" line="1107"/>
        <location filename="../mainwindow.ui" line="1234"/>
        <location filename="../mainwindow.cpp" line="2526"/>
        <location filename="../mainwindow.cpp" line="2557"/>
        <location filename="../mainwindow.cpp" line="2649"/>
        <location filename="../mainwindow.cpp" line="2650"/>
        <source>Not installed</source>
        <translation>Nincs telepítve</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="432"/>
        <location filename="../mainwindow.ui" line="752"/>
        <location filename="../mainwindow.ui" line="814"/>
        <source>= Upgradable package. Newer version available in selected repository.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="458"/>
        <location filename="../mainwindow.ui" line="650"/>
        <location filename="../mainwindow.ui" line="1020"/>
        <source>Package Name</source>
        <translation>Csomag neve</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="463"/>
        <location filename="../mainwindow.ui" line="655"/>
        <location filename="../mainwindow.ui" line="1025"/>
        <location filename="../mainwindow.ui" line="1469"/>
        <source>Version</source>
        <translation>Verzió</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="493"/>
        <source>MX Test Repo</source>
        <translation>MX teszt tároló</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="796"/>
        <source>Debian Backports</source>
        <translation>Debian visszaportolt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1142"/>
        <source>Flatpaks</source>
        <translation>Flatpackek</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1148"/>
        <location filename="../mainwindow.ui" line="1152"/>
        <source>For all users</source>
        <translation>Minden felhasználónak</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1157"/>
        <source>For current user</source>
        <translation>A jelenlegi felhasználónak</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1165"/>
        <source>Remote (repo):</source>
        <translation>Távoli (csomagforrás):</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1200"/>
        <location filename="../mainwindow.ui" line="1204"/>
        <location filename="../mainwindow.cpp" line="2508"/>
        <source>All apps</source>
        <translation>Minden alkalmazás</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1209"/>
        <location filename="../mainwindow.cpp" line="2512"/>
        <source>All runtimes</source>
        <translation>Minden futtatási környezet</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1214"/>
        <location filename="../mainwindow.cpp" line="2516"/>
        <source>All available</source>
        <translation>Minden elérhető</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1219"/>
        <location filename="../mainwindow.cpp" line="2506"/>
        <source>Installed apps</source>
        <translation>Telepített alkalmazások</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1224"/>
        <location filename="../mainwindow.cpp" line="2504"/>
        <source>Installed runtimes</source>
        <translation>Telepített futtatási környezetek</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1229"/>
        <location filename="../mainwindow.cpp" line="2524"/>
        <location filename="../mainwindow.cpp" line="2643"/>
        <location filename="../mainwindow.cpp" line="2644"/>
        <source>All installed</source>
        <translation>Minden telepített</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1316"/>
        <source>Total items </source>
        <translation>Összes elem</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1330"/>
        <source>Installed apps:</source>
        <translation>Telepített alkalmazások:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1383"/>
        <source>Advanced</source>
        <translation>Haladó</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1413"/>
        <source>Total installed size:</source>
        <translation>Teljese telepítési méret:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1427"/>
        <source>Remove unused runtimes</source>
        <translation>Nem használt futtatási környezetek eltávolítása</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1459"/>
        <source>Short Name</source>
        <translation>Rövid név</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1464"/>
        <source>Full Package Name</source>
        <translation>Csomag teljes neve</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1474"/>
        <source>Size</source>
        <translation>Méret</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1504"/>
        <location filename="../mainwindow.cpp" line="2315"/>
        <location filename="../mainwindow.cpp" line="2464"/>
        <source>Console Output</source>
        <translation>Konzol kimenet</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1510"/>
        <source>Enter</source>
        <translation>Enter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1521"/>
        <source>Respond here, or just press Enter</source>
        <translation>Itt válaszoljon, vagy csak nyomja meg az Enter billentyűt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1577"/>
        <location filename="../mainwindow.cpp" line="1075"/>
        <location filename="../mainwindow.cpp" line="2636"/>
        <location filename="../mainwindow.cpp" line="2640"/>
        <location filename="../mainwindow.cpp" line="2895"/>
        <source>Install</source>
        <translation>Telepítés</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1584"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1603"/>
        <source>Quit application</source>
        <translation>Kilépés az alkalmazásból</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1606"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1613"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1645"/>
        <source>About this application</source>
        <translation>Az alkalmazásról</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1648"/>
        <source>About...</source>
        <translation>Névjegy</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1655"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1693"/>
        <source>Display help </source>
        <translation>Súgó megjelenítése</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1696"/>
        <source>Help</source>
        <translation>Súgó</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1703"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1719"/>
        <source>Uninstall</source>
        <translation>Eltávolítás</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1726"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="91"/>
        <source>Flatpak tab is disabled on 32-bit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="184"/>
        <source>Uninstalling packages...</source>
        <translation>Csomagok eltávolítása...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="189"/>
        <source>Running pre-uninstall operations...</source>
        <translation>Eltávolítás előtti műveletek futtatása...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="204"/>
        <source>Running post-uninstall operations...</source>
        <translation>Eltávolítás utáni műveletek futtatása...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="218"/>
        <source>Refreshing sources...</source>
        <translation>Források frissítése...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="232"/>
        <location filename="../mainwindow.cpp" line="1121"/>
        <location filename="../mainwindow.cpp" line="1250"/>
        <location filename="../mainwindow.cpp" line="1392"/>
        <location filename="../mainwindow.cpp" line="1454"/>
        <location filename="../mainwindow.cpp" line="2132"/>
        <location filename="../mainwindow.cpp" line="2162"/>
        <location filename="../mainwindow.cpp" line="2174"/>
        <location filename="../mainwindow.cpp" line="2288"/>
        <location filename="../mainwindow.cpp" line="2307"/>
        <location filename="../mainwindow.cpp" line="2364"/>
        <location filename="../mainwindow.cpp" line="2381"/>
        <location filename="../mainwindow.cpp" line="2398"/>
        <location filename="../mainwindow.cpp" line="2719"/>
        <location filename="../mainwindow.cpp" line="2795"/>
        <location filename="../mainwindow.cpp" line="2825"/>
        <location filename="../mainwindow.cpp" line="2918"/>
        <location filename="../mainwindow.cpp" line="2942"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="233"/>
        <source>There was a problem updating sources. Some sources may not have provided updates. For more info check: </source>
        <translation>Probléma merült fel a források frissítésekor. Előfordulhat, hogy egyes források nem szolgáltattak frissítéseket. További információk:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="625"/>
        <source>Cancel</source>
        <translation>Mégsem</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="632"/>
        <source>Please wait...</source>
        <translation>Kérem várjon...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="814"/>
        <location filename="../mainwindow.cpp" line="831"/>
        <source>Version </source>
        <translation>Verzió</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="814"/>
        <source> in stable repo</source>
        <translation>stabil tárolóban</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="816"/>
        <source>Not available in stable repo</source>
        <translation>Nem érhető el stabil tárolóban</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="825"/>
        <location filename="../mainwindow.cpp" line="2441"/>
        <source>Latest version </source>
        <translation>Legfrissebb verzió</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="825"/>
        <location filename="../mainwindow.cpp" line="2441"/>
        <source> already installed</source>
        <translation>már telepítve van</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="831"/>
        <source> installed</source>
        <translation>telepítve</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="943"/>
        <source>You are about to use the MX Test repository, whose packages are provided for testing purposes only. It is possible that they might break your system, so it is suggested that you back up your system and install or update only one package at a time. Please provide feedback in the Forum so the package can be evaluated before moving up to Main.</source>
        <translation>Ön az MX Teszt csomagforrást fogja használni, amelynek csomagjai csak tesztelési célokra szolgálnak. Lehetséges, hogy ezek tönkretehetik a rendszerét, ezért javasoljuk, hogy készítsen biztonsági mentést, és egyszerre csak egy csomagot telepítsen vagy frissítsen. Kérjük, küldjön visszajelzést a Fórumban, hogy a csomagot kiértékelhessük, mielőtt a fő csomagforrásba kerülne.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="952"/>
        <source>You are about to use Debian Backports, which contains packages taken from the next Debian release (called &apos;testing&apos;), adjusted and recompiled for usage on Debian stable. They cannot be tested as extensively as in the stable releases of Debian and MX Linux, and are provided on an as-is basis, with risk of incompatibilities with other components in Debian stable. Use with care!</source>
        <translation>Ön most a Debian visszaportolt csomagforrást fogja használni, amely a következő Debian kiadásból (úgynevezett &quot;testing&quot;) származó csomagokat tartalmaz, amelyeket a Debian stabil használatához igazítottak és fordítottak újra. Ezeket nincsenek olyan alaposan tesztelve, mint a Debian és az MX Linux stabil kiadásai. A Debian stabil kiadás más összetevőivel való inkompatibilitás kockázata miatt, a csomagokat &quot;úgy, ahogy van&quot; alapon bocsátják rendelkezésre. Használja óvatosan!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="960"/>
        <source>MX Linux includes this repository of flatpaks for the users&apos; convenience only, and is not responsible for the functionality of the individual flatpaks themselves. For more, consult flatpaks in the Wiki.</source>
        <translation>Az MX Linux csak a felhasználók kényelmét szolgálja a flatpakek elérhetőségének biztosításával, és nem vállal felelősséget az egyes flatpakek működéséért. További információért nézze meg a flatpaks oldalt a Wikiben.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="967"/>
        <location filename="../mainwindow.cpp" line="2928"/>
        <source>Warning</source>
        <translation>Figyelmeztetés</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="971"/>
        <source>Do not show this message again</source>
        <translation>Ne jelen meg újra ez az üzenet</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1073"/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1087"/>
        <source>The following packages were selected. Click Show Details for list of changes.</source>
        <translation>A következő csomagokat választotta ki. Kattintson a Részletek megjelenítése gombra a változások listájához.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1122"/>
        <location filename="../mainwindow.cpp" line="1251"/>
        <location filename="../mainwindow.cpp" line="1455"/>
        <source>Internet is not available, won&apos;t be able to download the list of packages</source>
        <translation>Az internet nem érhető el, nem lesz lehetséges a csomaglista letöltése</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1127"/>
        <source>Installing packages...</source>
        <translation>Csomagok telepítése...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1184"/>
        <source>Post-processing...</source>
        <translation>Utófeldolgozás...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1214"/>
        <source>Pre-processing for </source>
        <translation>Előfeldolgozás ehhez:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1227"/>
        <source>Installing </source>
        <translation>Telepítés</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1234"/>
        <source>Post-processing for </source>
        <translation>Utófeldolgozás ehhez:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1393"/>
        <source>There was an error writing file: %1. Please check if you have enough free space on your drive</source>
        <translation>Hiba történt a fájl írása során: %1. Ellenőrizze, hogy van-e elég szabad hely a meghajtón.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1463"/>
        <source>Downloading package info...</source>
        <translation>Csomaginformációk letöltése...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1919"/>
        <location filename="../mainwindow.cpp" line="1996"/>
        <location filename="../mainwindow.cpp" line="2020"/>
        <source>Package info</source>
        <translation>Csomaginformációk</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1933"/>
        <location filename="../mainwindow.cpp" line="2861"/>
        <source>More &amp;info...</source>
        <translation>További &amp;infó...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1962"/>
        <source>Packages to be installed: </source>
        <translation>Telepítendő csomagok:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2128"/>
        <location filename="../mainwindow.cpp" line="2141"/>
        <location filename="../mainwindow.cpp" line="2156"/>
        <location filename="../mainwindow.cpp" line="2171"/>
        <location filename="../mainwindow.cpp" line="2260"/>
        <location filename="../mainwindow.cpp" line="2283"/>
        <location filename="../mainwindow.cpp" line="2715"/>
        <location filename="../mainwindow.cpp" line="2789"/>
        <location filename="../mainwindow.cpp" line="2819"/>
        <location filename="../mainwindow.cpp" line="2912"/>
        <source>Done</source>
        <translation>Kész</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2128"/>
        <location filename="../mainwindow.cpp" line="2141"/>
        <location filename="../mainwindow.cpp" line="2156"/>
        <location filename="../mainwindow.cpp" line="2171"/>
        <location filename="../mainwindow.cpp" line="2260"/>
        <location filename="../mainwindow.cpp" line="2283"/>
        <location filename="../mainwindow.cpp" line="2301"/>
        <location filename="../mainwindow.cpp" line="2715"/>
        <location filename="../mainwindow.cpp" line="2789"/>
        <location filename="../mainwindow.cpp" line="2819"/>
        <location filename="../mainwindow.cpp" line="2912"/>
        <location filename="../mainwindow.cpp" line="2936"/>
        <source>Processing finished successfully.</source>
        <translation>A feldolgozás sikeresen befejeződött.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2133"/>
        <location filename="../mainwindow.cpp" line="2163"/>
        <location filename="../mainwindow.cpp" line="2175"/>
        <location filename="../mainwindow.cpp" line="2720"/>
        <location filename="../mainwindow.cpp" line="2796"/>
        <location filename="../mainwindow.cpp" line="2826"/>
        <source>Problem detected while installing, please inspect the console output.</source>
        <translation>Probléma merült fel a telepítés során. Vizsgálja meg a konzol kimenetet.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2185"/>
        <source>About %1</source>
        <translation>%1 névjegye</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2186"/>
        <source>Version: </source>
        <translation>Verzió:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2188"/>
        <source>Package Installer for MX Linux</source>
        <translation>Csomagtelepítő MX Linuxhoz</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2190"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2191"/>
        <source>%1 License</source>
        <translation>%1 licenc</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2204"/>
        <source>%1 Help</source>
        <translation>%1 Súgó</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2288"/>
        <source>We encountered a problem uninstalling, please check output</source>
        <translation>Hiba történt az eltávolítás során. Vizsgálja meg a kimenetet.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2301"/>
        <location filename="../mainwindow.cpp" line="2936"/>
        <source>Success</source>
        <translation>Sikeres</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2307"/>
        <location filename="../mainwindow.cpp" line="2942"/>
        <source>We encountered a problem uninstalling the program</source>
        <translation>Hiba történt a program eltávolítása során</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2365"/>
        <location filename="../mainwindow.cpp" line="2382"/>
        <location filename="../mainwindow.cpp" line="2399"/>
        <source>Could not download the list of packages. Please check your APT sources.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2415"/>
        <location filename="../mainwindow.cpp" line="2427"/>
        <source>Flatpak not installed</source>
        <translation>Flatpak nincs telepítve</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2416"/>
        <source>Flatpak is not currently installed.
OK to go ahead and install it?</source>
        <translation>A Flatpak jelenleg nincs telepítve.
Indulhat a telepítése?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2427"/>
        <source>Flatpak was not installed</source>
        <translation>Flatpak nem került telepítésre</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2451"/>
        <location filename="../mainwindow.cpp" line="2473"/>
        <source>Flathub remote failed</source>
        <translation>Flathub forrás hiba</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2451"/>
        <location filename="../mainwindow.cpp" line="2473"/>
        <source>Flathub remote could not be added</source>
        <translation>Flathub forrás hozzáadása nem sikerült</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2459"/>
        <source>Needs re-login</source>
        <translation>Újra bejelentkezés szükséges</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2460"/>
        <source>You might need to logout/login to see installed items in the menu</source>
        <translation>Lehet, hogy ki kell jelentkeznie, majd ismét be, hogy láthassa a telepített elemeket a menüben</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2636"/>
        <source>Upgrade</source>
        <translation>Frissítés</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2748"/>
        <source>Quit?</source>
        <translation>Kilépés?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2749"/>
        <source>Process still running, quitting might leave the system in an unstable state.&lt;p&gt;&lt;b&gt;Are you sure you want to exit MX Package Installer?&lt;/b&gt;</source>
        <translation>A feldolgozás még folyamatban van. A kilépés instabil állapotba hozhatja a rendszert.&lt;p&gt;&lt;b&gt;Biztos, hogy ki szeretne lépni az MX csomagtelepítőből?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2893"/>
        <source>Reinstall</source>
        <translation>Újratelepítés</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2919"/>
        <source>Problem detected during last operation, please inspect the console output.</source>
        <translation>Probléma merült fel a legutóbbi műveletnél. Vizsgálja meg a konzol kimenetet.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2929"/>
        <source>Potentially dangerous operation.
Please make sure you check carefully the list of packages to be removed.</source>
        <translation>Potenciálisan veszélyes művelet.
Ellenőrizze alaposan az eltávolítandó csomagok listáját.</translation>
    </message>
</context>
<context>
    <name>ManageRemotes</name>
    <message>
        <location filename="../remotes.cpp" line="13"/>
        <source>Manage Flatpak Remotes</source>
        <translation>Flatpak források kezelése</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="22"/>
        <source>For all users</source>
        <translation>Minden felhasználónak</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="23"/>
        <source>For current user</source>
        <translation>A jelenlegi felhasználónak</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="29"/>
        <source>enter Flatpak remote URL</source>
        <translation>írja be a Flatpak forrás URL címét</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="32"/>
        <source>enter Flatpakref location to install app</source>
        <translation>írja be a Flatpakref helyét az alkalmazás telepítéséhez</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="34"/>
        <source>Add or remove flatpak remotes (repos), or install apps using flatpakref URL or path</source>
        <translation>Flatpak csomagforrások hozzáadása, eltávolítása vagy alkalmazás telepítése flatpakref URL címmel vagy útvonallal</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="43"/>
        <source>Remove remote</source>
        <translation>Forrás eltávolítása</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="48"/>
        <source>Add remote</source>
        <translation>Forrás hozzáadása</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="53"/>
        <source>Install app</source>
        <translation>Alkalmazás telepítése</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="58"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="75"/>
        <source>Not removable</source>
        <translation>Nem távolítható el</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="76"/>
        <source>Flathub is the main Flatpak remote and won&apos;t be removed</source>
        <translation>A Flathub a fő Flatpak forrás és nem törölhető</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="96"/>
        <source>Error adding remote</source>
        <translation>Hiba a forrás hozzáadásakor</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="97"/>
        <source>Could not add remote - command returned an error. Please double-check the remote address and try again</source>
        <translation>A forrás hozzáadása nem sikerült - a parancs hibával tért vissza. Ellenőrizze a forrás címét és próbálja meg újra.</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="102"/>
        <source>Success</source>
        <translation>Sikeres</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="102"/>
        <source>Remote added successfully</source>
        <translation>A forrás hozzáadása sikeres</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../about.cpp" line="52"/>
        <source>License</source>
        <translation>Licenc</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="53"/>
        <location filename="../about.cpp" line="62"/>
        <source>Changelog</source>
        <translation>Változások listája</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="54"/>
        <source>Cancel</source>
        <translation>Mégsem</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>&amp;Bezárás</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>MX Package Installer is a tool used for managing packages on MX Linux
    - installs popular programs from different sources
    - installs programs from the MX Test repo
    - installs programs from Debian Backports repo
    - installs flatpaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="82"/>
        <source>Skip online check if it falsely reports lack of internet access.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="88"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="89"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Úgy tűnik, hogy root felhasználóként van bejelentkezve. Jelentkezzen ki és jelentkezzen be normál felhasználóként a program használatához.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="99"/>
        <source>Unable to get exclusive lock</source>
        <translation>Zárolás sikertelen</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="100"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>Egy másik csomagkezelő alkalmazás (például a Synaptic, vagy az apt-get) már fut. Először zárja be azt az alkalmazást</translation>
    </message>
</context>
</TS>
