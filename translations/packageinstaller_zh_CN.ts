<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <location filename="../mainwindow.cpp" line="100"/>
        <source>MX Package Installer</source>
        <translation>MX 软件包安装器</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="57"/>
        <source>Popular Applications</source>
        <translation>常用的应用程序</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="73"/>
        <source>Category</source>
        <translation>分类</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="83"/>
        <source>Package</source>
        <translation>软件包名</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="88"/>
        <source>Info</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="93"/>
        <location filename="../mainwindow.ui" line="468"/>
        <location filename="../mainwindow.ui" line="660"/>
        <location filename="../mainwindow.ui" line="1030"/>
        <source>Description</source>
        <translation>具体描述</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="101"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Manage popular packages&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;管理热门软件包&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="120"/>
        <location filename="../mainwindow.ui" line="191"/>
        <location filename="../mainwindow.ui" line="730"/>
        <location filename="../mainwindow.ui" line="1055"/>
        <location filename="../mainwindow.ui" line="1274"/>
        <source>search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="149"/>
        <location filename="../mainwindow.ui" line="204"/>
        <location filename="../mainwindow.ui" line="772"/>
        <location filename="../mainwindow.ui" line="1118"/>
        <location filename="../mainwindow.ui" line="1245"/>
        <source>= Installed packages</source>
        <translation>= 已安装软件包</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="173"/>
        <source>Enabled Repos</source>
        <translation>已启用仓库</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="237"/>
        <source>Remove orphan packages</source>
        <translation>移除孤立包</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="264"/>
        <location filename="../mainwindow.ui" line="1337"/>
        <source>Upgrade All</source>
        <translation>升级所有</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="278"/>
        <location filename="../mainwindow.ui" line="558"/>
        <location filename="../mainwindow.ui" line="886"/>
        <source>Installed:</source>
        <translation>已安装：</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="301"/>
        <location filename="../mainwindow.ui" line="508"/>
        <location filename="../mainwindow.ui" line="907"/>
        <source>Total packages:</source>
        <translation>软件包总数：</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="315"/>
        <location filename="../mainwindow.ui" line="615"/>
        <location filename="../mainwindow.ui" line="947"/>
        <source>Also Install &quot;Recommended&quot; Packages</source>
        <translation>同时安装“推荐”包</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="322"/>
        <location filename="../mainwindow.ui" line="588"/>
        <location filename="../mainwindow.ui" line="879"/>
        <source>Upgradable:</source>
        <translation>可升级：</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="329"/>
        <location filename="../mainwindow.ui" line="522"/>
        <location filename="../mainwindow.ui" line="914"/>
        <source>Hide library and developer packages</source>
        <translation>隐藏库与开发软件包</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="351"/>
        <location filename="../mainwindow.ui" line="544"/>
        <location filename="../mainwindow.ui" line="966"/>
        <source>Refresh list</source>
        <translation>刷新列表</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="390"/>
        <location filename="../mainwindow.ui" line="688"/>
        <location filename="../mainwindow.ui" line="1085"/>
        <location filename="../mainwindow.ui" line="1197"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filter packages according to their status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;根据软件包的状态过滤。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="393"/>
        <location filename="../mainwindow.ui" line="397"/>
        <location filename="../mainwindow.ui" line="691"/>
        <location filename="../mainwindow.ui" line="695"/>
        <location filename="../mainwindow.ui" line="1088"/>
        <location filename="../mainwindow.ui" line="1092"/>
        <location filename="../mainwindow.cpp" line="2541"/>
        <source>All packages</source>
        <translation>所有软件包</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="402"/>
        <location filename="../mainwindow.ui" line="700"/>
        <location filename="../mainwindow.ui" line="1097"/>
        <location filename="../mainwindow.cpp" line="2554"/>
        <source>Installed</source>
        <translation>已安装软件包</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <location filename="../mainwindow.ui" line="705"/>
        <location filename="../mainwindow.ui" line="1102"/>
        <location filename="../mainwindow.cpp" line="2552"/>
        <source>Upgradable</source>
        <translation>可升级软件包</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="412"/>
        <location filename="../mainwindow.ui" line="710"/>
        <location filename="../mainwindow.ui" line="1107"/>
        <location filename="../mainwindow.ui" line="1234"/>
        <location filename="../mainwindow.cpp" line="2526"/>
        <location filename="../mainwindow.cpp" line="2557"/>
        <location filename="../mainwindow.cpp" line="2649"/>
        <location filename="../mainwindow.cpp" line="2650"/>
        <source>Not installed</source>
        <translation>未安装软件包</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="432"/>
        <location filename="../mainwindow.ui" line="752"/>
        <location filename="../mainwindow.ui" line="814"/>
        <source>= Upgradable package. Newer version available in selected repository.</source>
        <translation>= 可升级软件包。所选仓库中有新版本可用。</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="458"/>
        <location filename="../mainwindow.ui" line="650"/>
        <location filename="../mainwindow.ui" line="1020"/>
        <source>Package Name</source>
        <translation>软件包名</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="463"/>
        <location filename="../mainwindow.ui" line="655"/>
        <location filename="../mainwindow.ui" line="1025"/>
        <location filename="../mainwindow.ui" line="1469"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="493"/>
        <source>MX Test Repo</source>
        <translation>MX 测试仓库</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="796"/>
        <source>Debian Backports</source>
        <translation>Debian Backports</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1142"/>
        <source>Flatpaks</source>
        <translation>Flatpaks</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1148"/>
        <location filename="../mainwindow.ui" line="1152"/>
        <source>For all users</source>
        <translation>为所有用户</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1157"/>
        <source>For current user</source>
        <translation>为当前用户</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1165"/>
        <source>Remote (repo):</source>
        <translation>远程（仓库）：</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1200"/>
        <location filename="../mainwindow.ui" line="1204"/>
        <location filename="../mainwindow.cpp" line="2508"/>
        <source>All apps</source>
        <translation>所有程序</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1209"/>
        <location filename="../mainwindow.cpp" line="2512"/>
        <source>All runtimes</source>
        <translation>所有运行库</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1214"/>
        <location filename="../mainwindow.cpp" line="2516"/>
        <source>All available</source>
        <translation>所有可用程序</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1219"/>
        <location filename="../mainwindow.cpp" line="2506"/>
        <source>Installed apps</source>
        <translation>已安装软件</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1224"/>
        <location filename="../mainwindow.cpp" line="2504"/>
        <source>Installed runtimes</source>
        <translation>已安装运行库</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1229"/>
        <location filename="../mainwindow.cpp" line="2524"/>
        <location filename="../mainwindow.cpp" line="2643"/>
        <location filename="../mainwindow.cpp" line="2644"/>
        <source>All installed</source>
        <translation>已全部安装</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1316"/>
        <source>Total items </source>
        <translation>总数</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1330"/>
        <source>Installed apps:</source>
        <translation>已安装软件：</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1383"/>
        <source>Advanced</source>
        <translation>高级</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1413"/>
        <source>Total installed size:</source>
        <translation>软件包总大小：</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1427"/>
        <source>Remove unused runtimes</source>
        <translation>移除未使用的运行时</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1459"/>
        <source>Short Name</source>
        <translation>简略名称</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1464"/>
        <source>Full Package Name</source>
        <translation>软件包全名</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1474"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1504"/>
        <location filename="../mainwindow.cpp" line="2315"/>
        <location filename="../mainwindow.cpp" line="2464"/>
        <source>Console Output</source>
        <translation>控制台输出</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1510"/>
        <source>Enter</source>
        <translation>回车</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1521"/>
        <source>Respond here, or just press Enter</source>
        <translation>再次进行回应，或按回车</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1577"/>
        <location filename="../mainwindow.cpp" line="1075"/>
        <location filename="../mainwindow.cpp" line="2636"/>
        <location filename="../mainwindow.cpp" line="2640"/>
        <location filename="../mainwindow.cpp" line="2895"/>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1584"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1603"/>
        <source>Quit application</source>
        <translation>退出应用程序</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1606"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1613"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1645"/>
        <source>About this application</source>
        <translation>关于此软件</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1648"/>
        <source>About...</source>
        <translation>关于…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1655"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1693"/>
        <source>Display help </source>
        <translation>显示帮助</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1696"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1703"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1719"/>
        <source>Uninstall</source>
        <translation>卸载</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1726"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="91"/>
        <source>Flatpak tab is disabled on 32-bit.</source>
        <translation>Flatpak 选项卡在 32 位系统中被禁用。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="184"/>
        <source>Uninstalling packages...</source>
        <translation>卸载软件包中…</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="189"/>
        <source>Running pre-uninstall operations...</source>
        <translation>正在运行卸载前操作...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="204"/>
        <source>Running post-uninstall operations...</source>
        <translation>正在运行卸载后操作……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="218"/>
        <source>Refreshing sources...</source>
        <translation>刷新软件源中……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="232"/>
        <location filename="../mainwindow.cpp" line="1121"/>
        <location filename="../mainwindow.cpp" line="1250"/>
        <location filename="../mainwindow.cpp" line="1392"/>
        <location filename="../mainwindow.cpp" line="1454"/>
        <location filename="../mainwindow.cpp" line="2132"/>
        <location filename="../mainwindow.cpp" line="2162"/>
        <location filename="../mainwindow.cpp" line="2174"/>
        <location filename="../mainwindow.cpp" line="2288"/>
        <location filename="../mainwindow.cpp" line="2307"/>
        <location filename="../mainwindow.cpp" line="2364"/>
        <location filename="../mainwindow.cpp" line="2381"/>
        <location filename="../mainwindow.cpp" line="2398"/>
        <location filename="../mainwindow.cpp" line="2719"/>
        <location filename="../mainwindow.cpp" line="2795"/>
        <location filename="../mainwindow.cpp" line="2825"/>
        <location filename="../mainwindow.cpp" line="2918"/>
        <location filename="../mainwindow.cpp" line="2942"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="233"/>
        <source>There was a problem updating sources. Some sources may not have provided updates. For more info check: </source>
        <translation>更新软件源中出现错误。一些软件源或许不能提供更新。更多信息请查看：</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="625"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="632"/>
        <source>Please wait...</source>
        <translation>请稍等……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="814"/>
        <location filename="../mainwindow.cpp" line="831"/>
        <source>Version </source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="814"/>
        <source> in stable repo</source>
        <translation>在 stable 软件仓库中</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="816"/>
        <source>Not available in stable repo</source>
        <translation>在 stable 软件仓库中不可用</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="825"/>
        <location filename="../mainwindow.cpp" line="2441"/>
        <source>Latest version </source>
        <translation>最新版本</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="825"/>
        <location filename="../mainwindow.cpp" line="2441"/>
        <source> already installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="831"/>
        <source> installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="943"/>
        <source>You are about to use the MX Test repository, whose packages are provided for testing purposes only. It is possible that they might break your system, so it is suggested that you back up your system and install or update only one package at a time. Please provide feedback in the Forum so the package can be evaluated before moving up to Main.</source>
        <translation>您将要使用 MX 测试软件仓库，其中软件包仅为测试目的提供。它们有可能破坏您的系统，故我们谨以您备份您的系统，并一次只安装或升级一个软件包。请在论坛中进行反馈，以便在这些软件包移至主仓库之前进行评估。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="952"/>
        <source>You are about to use Debian Backports, which contains packages taken from the next Debian release (called &apos;testing&apos;), adjusted and recompiled for usage on Debian stable. They cannot be tested as extensively as in the stable releases of Debian and MX Linux, and are provided on an as-is basis, with risk of incompatibilities with other components in Debian stable. Use with care!</source>
        <translation>您将要使用 Debian Backports 更新源，这其中包含了来自下一个 Debian 版本（即 “testing”）的软件包，他们被调整与重编译以便可以在 Debian stable 上使用。它们不能像 Debian 和 MX Linux 的稳定版那样进行充分的测试，且在软件包原样的基础上提供，它们可能与 Debian stable 的其他组件不兼容。小心使用！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="960"/>
        <source>MX Linux includes this repository of flatpaks for the users&apos; convenience only, and is not responsible for the functionality of the individual flatpaks themselves. For more, consult flatpaks in the Wiki.</source>
        <translation>MX Linux 包含该 Flatpaks 仓库仅为方便用户使用，并不对单个 Flatpaks 本身的功能负责。更多信息，参见维基中的 flatpaks 页面。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="967"/>
        <location filename="../mainwindow.cpp" line="2928"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="971"/>
        <source>Do not show this message again</source>
        <translation>不再显示这条信息</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1073"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1087"/>
        <source>The following packages were selected. Click Show Details for list of changes.</source>
        <translation>已选择以下软件包。单击“显示细节”列出变更列表。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1122"/>
        <location filename="../mainwindow.cpp" line="1251"/>
        <location filename="../mainwindow.cpp" line="1455"/>
        <source>Internet is not available, won&apos;t be able to download the list of packages</source>
        <translation>网络不可用，无法下载以下软件包</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1127"/>
        <source>Installing packages...</source>
        <translation>安装软件包中……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1184"/>
        <source>Post-processing...</source>
        <translation>安装后设置中……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1214"/>
        <source>Pre-processing for </source>
        <translation>正在安装前设置</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1227"/>
        <source>Installing </source>
        <translation>正在安装</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1234"/>
        <source>Post-processing for </source>
        <translation>正在安装后设置</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1393"/>
        <source>There was an error writing file: %1. Please check if you have enough free space on your drive</source>
        <translation>写入文件时出错：%1。请检查您的驱动器上是否有足够的可用空间</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1463"/>
        <source>Downloading package info...</source>
        <translation>下载软件包信息中……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1919"/>
        <location filename="../mainwindow.cpp" line="1996"/>
        <location filename="../mainwindow.cpp" line="2020"/>
        <source>Package info</source>
        <translation>软件包信息</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1933"/>
        <location filename="../mainwindow.cpp" line="2861"/>
        <source>More &amp;info...</source>
        <translation>更多信息(&amp;i)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1962"/>
        <source>Packages to be installed: </source>
        <translation>将要安装的软件包：</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2128"/>
        <location filename="../mainwindow.cpp" line="2141"/>
        <location filename="../mainwindow.cpp" line="2156"/>
        <location filename="../mainwindow.cpp" line="2171"/>
        <location filename="../mainwindow.cpp" line="2260"/>
        <location filename="../mainwindow.cpp" line="2283"/>
        <location filename="../mainwindow.cpp" line="2715"/>
        <location filename="../mainwindow.cpp" line="2789"/>
        <location filename="../mainwindow.cpp" line="2819"/>
        <location filename="../mainwindow.cpp" line="2912"/>
        <source>Done</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2128"/>
        <location filename="../mainwindow.cpp" line="2141"/>
        <location filename="../mainwindow.cpp" line="2156"/>
        <location filename="../mainwindow.cpp" line="2171"/>
        <location filename="../mainwindow.cpp" line="2260"/>
        <location filename="../mainwindow.cpp" line="2283"/>
        <location filename="../mainwindow.cpp" line="2301"/>
        <location filename="../mainwindow.cpp" line="2715"/>
        <location filename="../mainwindow.cpp" line="2789"/>
        <location filename="../mainwindow.cpp" line="2819"/>
        <location filename="../mainwindow.cpp" line="2912"/>
        <location filename="../mainwindow.cpp" line="2936"/>
        <source>Processing finished successfully.</source>
        <translation>配置成功完成。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2133"/>
        <location filename="../mainwindow.cpp" line="2163"/>
        <location filename="../mainwindow.cpp" line="2175"/>
        <location filename="../mainwindow.cpp" line="2720"/>
        <location filename="../mainwindow.cpp" line="2796"/>
        <location filename="../mainwindow.cpp" line="2826"/>
        <source>Problem detected while installing, please inspect the console output.</source>
        <translation>安装过程中检测到问题，请查看控制台输出。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2185"/>
        <source>About %1</source>
        <translation>关于 %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2186"/>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2188"/>
        <source>Package Installer for MX Linux</source>
        <translation>MX Linux 的软件包安装器</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2190"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2191"/>
        <source>%1 License</source>
        <translation>%1 许可证</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2204"/>
        <source>%1 Help</source>
        <translation>%1 帮助</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2288"/>
        <source>We encountered a problem uninstalling, please check output</source>
        <translation>在卸载软件包时遇到了错误，请检查输出</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2301"/>
        <location filename="../mainwindow.cpp" line="2936"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2307"/>
        <location filename="../mainwindow.cpp" line="2942"/>
        <source>We encountered a problem uninstalling the program</source>
        <translation>在卸载程序过程中遇到了问题</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2365"/>
        <location filename="../mainwindow.cpp" line="2382"/>
        <location filename="../mainwindow.cpp" line="2399"/>
        <source>Could not download the list of packages. Please check your APT sources.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2415"/>
        <location filename="../mainwindow.cpp" line="2427"/>
        <source>Flatpak not installed</source>
        <translation>Flatpak 未安装</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2416"/>
        <source>Flatpak is not currently installed.
OK to go ahead and install it?</source>
        <translation>Flatpak 当前未安装
是否要继续并安装？</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2427"/>
        <source>Flatpak was not installed</source>
        <translation>Flatpak 未被安装</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2451"/>
        <location filename="../mainwindow.cpp" line="2473"/>
        <source>Flathub remote failed</source>
        <translation>Flathub 远程仓库连接失败</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2451"/>
        <location filename="../mainwindow.cpp" line="2473"/>
        <source>Flathub remote could not be added</source>
        <translation>Flathub 远程仓库无法被添加</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2459"/>
        <source>Needs re-login</source>
        <translation>需要重新登陆</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2460"/>
        <source>You might need to logout/login to see installed items in the menu</source>
        <translation>您也许需要 登出/登陆 以在菜单中看见所安装项目</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2636"/>
        <source>Upgrade</source>
        <translation>升级</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2748"/>
        <source>Quit?</source>
        <translation>退出？</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2749"/>
        <source>Process still running, quitting might leave the system in an unstable state.&lt;p&gt;&lt;b&gt;Are you sure you want to exit MX Package Installer?&lt;/b&gt;</source>
        <translation>进程仍在运行，退出可能会使系统处于不稳定状态。&lt;p&gt;&lt;b&gt;您确定要退出 MX 软件包安装器吗？&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2893"/>
        <source>Reinstall</source>
        <translation>重装</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2919"/>
        <source>Problem detected during last operation, please inspect the console output.</source>
        <translation>上次操作期间检测到问题，请检查控制台输出。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2929"/>
        <source>Potentially dangerous operation.
Please make sure you check carefully the list of packages to be removed.</source>
        <translation>有潜在危险的操作。
请确保仔细检查要移除的软件包列表。</translation>
    </message>
</context>
<context>
    <name>ManageRemotes</name>
    <message>
        <location filename="../remotes.cpp" line="13"/>
        <source>Manage Flatpak Remotes</source>
        <translation>管理 Flatpak 仓库</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="22"/>
        <source>For all users</source>
        <translation>为所有用户</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="23"/>
        <source>For current user</source>
        <translation>为当前用户</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="29"/>
        <source>enter Flatpak remote URL</source>
        <translation>输入 Flatpak 仓库网址</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="32"/>
        <source>enter Flatpakref location to install app</source>
        <translation>输入 Flatpakref 位置以安装应用程序</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="34"/>
        <source>Add or remove flatpak remotes (repos), or install apps using flatpakref URL or path</source>
        <translation>添加或移除 flatpak 仓库，或使用 flatpakref 网址或路径安装应用程序</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="43"/>
        <source>Remove remote</source>
        <translation>移除仓库</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="48"/>
        <source>Add remote</source>
        <translation>添加仓库</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="53"/>
        <source>Install app</source>
        <translation>安装应用程序</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="58"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="75"/>
        <source>Not removable</source>
        <translation>无法移除</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="76"/>
        <source>Flathub is the main Flatpak remote and won&apos;t be removed</source>
        <translation>Flathub 是主 Flatpak 仓库，不会被移除</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="96"/>
        <source>Error adding remote</source>
        <translation>添加仓库时出错</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="97"/>
        <source>Could not add remote - command returned an error. Please double-check the remote address and try again</source>
        <translation>无法添加仓库 - 命令返回错误。请仔细检查仓库地址并重试</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="102"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="102"/>
        <source>Remote added successfully</source>
        <translation>仓库已添加成功</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../about.cpp" line="52"/>
        <source>License</source>
        <translation>许可证</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="53"/>
        <location filename="../about.cpp" line="62"/>
        <source>Changelog</source>
        <translation>更新日志</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="54"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>关闭(&amp;C)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>MX Package Installer is a tool used for managing packages on MX Linux
    - installs popular programs from different sources
    - installs programs from the MX Test repo
    - installs programs from Debian Backports repo
    - installs flatpaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="82"/>
        <source>Skip online check if it falsely reports lack of internet access.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="88"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="89"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>您似乎是以 root 身份登录的，请注销并以普通用户身份登录以使用此程序。</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="99"/>
        <source>Unable to get exclusive lock</source>
        <translation>无法获得锁</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="100"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>其他的包管理器（如新立得包管理器或 apt-get）正在运行。请先关闭它们</translation>
    </message>
</context>
</TS>
