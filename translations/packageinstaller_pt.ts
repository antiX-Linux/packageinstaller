<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <location filename="../mainwindow.cpp" line="100"/>
        <source>MX Package Installer</source>
        <translation>MX Instalador de Pacotes</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="57"/>
        <source>Popular Applications</source>
        <translation>Aplicações selecionadas</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="73"/>
        <source>Category</source>
        <translation>Categoria</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="83"/>
        <source>Package</source>
        <translation>Pacote</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="88"/>
        <source>Info</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="93"/>
        <location filename="../mainwindow.ui" line="468"/>
        <location filename="../mainwindow.ui" line="660"/>
        <location filename="../mainwindow.ui" line="1030"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="101"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Manage popular packages&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Gerir pacotes populares&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="120"/>
        <location filename="../mainwindow.ui" line="191"/>
        <location filename="../mainwindow.ui" line="730"/>
        <location filename="../mainwindow.ui" line="1055"/>
        <location filename="../mainwindow.ui" line="1274"/>
        <source>search</source>
        <translation>procurar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="149"/>
        <location filename="../mainwindow.ui" line="204"/>
        <location filename="../mainwindow.ui" line="772"/>
        <location filename="../mainwindow.ui" line="1118"/>
        <location filename="../mainwindow.ui" line="1245"/>
        <source>= Installed packages</source>
        <translation>= Pacotes instalados</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="173"/>
        <source>Enabled Repos</source>
        <translation>Repositórios ativados</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="237"/>
        <source>Remove orphan packages</source>
        <translation>Remover pacotes órfãos</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="264"/>
        <location filename="../mainwindow.ui" line="1337"/>
        <source>Upgrade All</source>
        <translation>Atualizar tudo</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="278"/>
        <location filename="../mainwindow.ui" line="558"/>
        <location filename="../mainwindow.ui" line="886"/>
        <source>Installed:</source>
        <translation>Instalado:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="301"/>
        <location filename="../mainwindow.ui" line="508"/>
        <location filename="../mainwindow.ui" line="907"/>
        <source>Total packages:</source>
        <translation>Total de pacotes:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="315"/>
        <location filename="../mainwindow.ui" line="615"/>
        <location filename="../mainwindow.ui" line="947"/>
        <source>Also Install &quot;Recommended&quot; Packages</source>
        <translation>Instalar também pacotes &quot;Recomendados&quot;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="322"/>
        <location filename="../mainwindow.ui" line="588"/>
        <location filename="../mainwindow.ui" line="879"/>
        <source>Upgradable:</source>
        <translation>Atualizáveis:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="329"/>
        <location filename="../mainwindow.ui" line="522"/>
        <location filename="../mainwindow.ui" line="914"/>
        <source>Hide library and developer packages</source>
        <translation>Ocultar pacotes de bibliotecas e de programador</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="351"/>
        <location filename="../mainwindow.ui" line="544"/>
        <location filename="../mainwindow.ui" line="966"/>
        <source>Refresh list</source>
        <translation>Recarregar a lista</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="390"/>
        <location filename="../mainwindow.ui" line="688"/>
        <location filename="../mainwindow.ui" line="1085"/>
        <location filename="../mainwindow.ui" line="1197"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filter packages according to their status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filtrar pacotes pelo estado.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="393"/>
        <location filename="../mainwindow.ui" line="397"/>
        <location filename="../mainwindow.ui" line="691"/>
        <location filename="../mainwindow.ui" line="695"/>
        <location filename="../mainwindow.ui" line="1088"/>
        <location filename="../mainwindow.ui" line="1092"/>
        <location filename="../mainwindow.cpp" line="2541"/>
        <source>All packages</source>
        <translation>Todos os pacotes</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="402"/>
        <location filename="../mainwindow.ui" line="700"/>
        <location filename="../mainwindow.ui" line="1097"/>
        <location filename="../mainwindow.cpp" line="2554"/>
        <source>Installed</source>
        <translation>Instalado</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <location filename="../mainwindow.ui" line="705"/>
        <location filename="../mainwindow.ui" line="1102"/>
        <location filename="../mainwindow.cpp" line="2552"/>
        <source>Upgradable</source>
        <translation>Atualizáveis</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="412"/>
        <location filename="../mainwindow.ui" line="710"/>
        <location filename="../mainwindow.ui" line="1107"/>
        <location filename="../mainwindow.ui" line="1234"/>
        <location filename="../mainwindow.cpp" line="2526"/>
        <location filename="../mainwindow.cpp" line="2557"/>
        <location filename="../mainwindow.cpp" line="2649"/>
        <location filename="../mainwindow.cpp" line="2650"/>
        <source>Not installed</source>
        <translation>Não instalado</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="432"/>
        <location filename="../mainwindow.ui" line="752"/>
        <location filename="../mainwindow.ui" line="814"/>
        <source>= Upgradable package. Newer version available in selected repository.</source>
        <translation>= Pacote atualizável. Versão mais recente disponível no repositório selecionado.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="458"/>
        <location filename="../mainwindow.ui" line="650"/>
        <location filename="../mainwindow.ui" line="1020"/>
        <source>Package Name</source>
        <translation>Nome do pacote</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="463"/>
        <location filename="../mainwindow.ui" line="655"/>
        <location filename="../mainwindow.ui" line="1025"/>
        <location filename="../mainwindow.ui" line="1469"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="493"/>
        <source>MX Test Repo</source>
        <translation>MX Teste</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="796"/>
        <source>Debian Backports</source>
        <translation>Debian Backports</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1142"/>
        <source>Flatpaks</source>
        <translation>Flatpaks</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1148"/>
        <location filename="../mainwindow.ui" line="1152"/>
        <source>For all users</source>
        <translation>Para todos os utilizadores</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1157"/>
        <source>For current user</source>
        <translation>Para o utilizador atual</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1165"/>
        <source>Remote (repo):</source>
        <translation>Remoto (repositório):</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1200"/>
        <location filename="../mainwindow.ui" line="1204"/>
        <location filename="../mainwindow.cpp" line="2508"/>
        <source>All apps</source>
        <translation>Aplicações</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1209"/>
        <location filename="../mainwindow.cpp" line="2512"/>
        <source>All runtimes</source>
        <translation>Software &apos;runtime&apos;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1214"/>
        <location filename="../mainwindow.cpp" line="2516"/>
        <source>All available</source>
        <translation>Tudo</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1219"/>
        <location filename="../mainwindow.cpp" line="2506"/>
        <source>Installed apps</source>
        <translation>Aplicações instaladas</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1224"/>
        <location filename="../mainwindow.cpp" line="2504"/>
        <source>Installed runtimes</source>
        <translation>Software &apos;runtime&apos; instalado</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1229"/>
        <location filename="../mainwindow.cpp" line="2524"/>
        <location filename="../mainwindow.cpp" line="2643"/>
        <location filename="../mainwindow.cpp" line="2644"/>
        <source>All installed</source>
        <translation>Tudo instalado</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1316"/>
        <source>Total items </source>
        <translation>Total de itens </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1330"/>
        <source>Installed apps:</source>
        <translation>Aplicações instaladas:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1383"/>
        <source>Advanced</source>
        <translation>Gestão avançada</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1413"/>
        <source>Total installed size:</source>
        <translation>Total instalado:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1427"/>
        <source>Remove unused runtimes</source>
        <translation>Remover tempos de execução não utilizados</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1459"/>
        <source>Short Name</source>
        <translation>Nome curto</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1464"/>
        <source>Full Package Name</source>
        <translation>Nome completo do pacote</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1474"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1504"/>
        <location filename="../mainwindow.cpp" line="2315"/>
        <location filename="../mainwindow.cpp" line="2464"/>
        <source>Console Output</source>
        <translation>Processamento</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1510"/>
        <source>Enter</source>
        <translation>Introduzir/Enter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1521"/>
        <source>Respond here, or just press Enter</source>
        <translation>Responder aqui ou premir Introduzir/Enter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1577"/>
        <location filename="../mainwindow.cpp" line="1075"/>
        <location filename="../mainwindow.cpp" line="2636"/>
        <location filename="../mainwindow.cpp" line="2640"/>
        <location filename="../mainwindow.cpp" line="2895"/>
        <source>Install</source>
        <translation>Instalar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1584"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1603"/>
        <source>Quit application</source>
        <translation>Sair da aplicação</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1606"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1613"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1645"/>
        <source>About this application</source>
        <translation>Sobre esta aplicação</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1648"/>
        <source>About...</source>
        <translation>Sobre...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1655"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1693"/>
        <source>Display help </source>
        <translation>Mostrar ajuda </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1696"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1703"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1719"/>
        <source>Uninstall</source>
        <translation>Desinstalar</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1726"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="91"/>
        <source>Flatpak tab is disabled on 32-bit.</source>
        <translation>O separador Flatpak está desativado em 32 bits.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="184"/>
        <source>Uninstalling packages...</source>
        <translation>A desinstalar pacotes...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="189"/>
        <source>Running pre-uninstall operations...</source>
        <translation>A executar operações pré-desinstalação...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="204"/>
        <source>Running post-uninstall operations...</source>
        <translation>A executar operações pós-desinstalação...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="218"/>
        <source>Refreshing sources...</source>
        <translation>A actualizar as origens...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="232"/>
        <location filename="../mainwindow.cpp" line="1121"/>
        <location filename="../mainwindow.cpp" line="1250"/>
        <location filename="../mainwindow.cpp" line="1392"/>
        <location filename="../mainwindow.cpp" line="1454"/>
        <location filename="../mainwindow.cpp" line="2132"/>
        <location filename="../mainwindow.cpp" line="2162"/>
        <location filename="../mainwindow.cpp" line="2174"/>
        <location filename="../mainwindow.cpp" line="2288"/>
        <location filename="../mainwindow.cpp" line="2307"/>
        <location filename="../mainwindow.cpp" line="2364"/>
        <location filename="../mainwindow.cpp" line="2381"/>
        <location filename="../mainwindow.cpp" line="2398"/>
        <location filename="../mainwindow.cpp" line="2719"/>
        <location filename="../mainwindow.cpp" line="2795"/>
        <location filename="../mainwindow.cpp" line="2825"/>
        <location filename="../mainwindow.cpp" line="2918"/>
        <location filename="../mainwindow.cpp" line="2942"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="233"/>
        <source>There was a problem updating sources. Some sources may not have provided updates. For more info check: </source>
        <translation>Houve um problema ao actualizar as origens. Algumas origens podem não ter fornecido actualizações. Para mais informação, ver: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="625"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="632"/>
        <source>Please wait...</source>
        <translation>Aguardar...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="814"/>
        <location filename="../mainwindow.cpp" line="831"/>
        <source>Version </source>
        <translation>Versão </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="814"/>
        <source> in stable repo</source>
        <translation> nos Estáveis</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="816"/>
        <source>Not available in stable repo</source>
        <translation>Não disponível nos Estáveis</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="825"/>
        <location filename="../mainwindow.cpp" line="2441"/>
        <source>Latest version </source>
        <translation>Última versão </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="825"/>
        <location filename="../mainwindow.cpp" line="2441"/>
        <source> already installed</source>
        <translation> já instalada</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="831"/>
        <source> installed</source>
        <translation> instalada</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="943"/>
        <source>You are about to use the MX Test repository, whose packages are provided for testing purposes only. It is possible that they might break your system, so it is suggested that you back up your system and install or update only one package at a time. Please provide feedback in the Forum so the package can be evaluated before moving up to Main.</source>
        <translation>Está prestes a ser usado o repositório MX Teste, cujos pacotes são disponibilizados sobretudo para serem testados. Há alguma probabilidade de os pacotes deste repositório danificarem o sistema, pelo que é sugerido que seja feita uma cópia de segurança (backup) do sistema e que seja instalado ou actualizado apenas um pacote de cada vez. Solicita-se que os resultados (execução sem problemas ou com problemas e quais) sejam comunicados no fórum (https://forum.mxlinux.org, em &apos;Select language&apos; escolher Portuguese, registar-se se ainda não registado, e ir para a secção Comunidade), para que seja feita uma re-avaliação dos pacotes antes de serem movidos para o repositório estável.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="952"/>
        <source>You are about to use Debian Backports, which contains packages taken from the next Debian release (called &apos;testing&apos;), adjusted and recompiled for usage on Debian stable. They cannot be tested as extensively as in the stable releases of Debian and MX Linux, and are provided on an as-is basis, with risk of incompatibilities with other components in Debian stable. Use with care!</source>
        <translation>Está prestes a ser usado o repositório Debian Retroportados. Este repositório contém pacotes retirados da versão Debian &apos;testing&apos; (próxima versão oficial do Debian), adaptados e recompilados para uso na actual versão oficial (Debian &apos;stable&apos;) - sendo por isso designados de pacotes retroportados. Estes pacotes não foram tão extensivamente testados como os das versões estáveis do Debian e do MX; por isso, são disponibilizados &apos;como estão&apos;, com o risco de incompatibilidades com outros componentes do Debian &apos;stable&apos;. Usar com cuidado!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="960"/>
        <source>MX Linux includes this repository of flatpaks for the users&apos; convenience only, and is not responsible for the functionality of the individual flatpaks themselves. For more, consult flatpaks in the Wiki.</source>
        <translation>O MX inclui este repositório de pacotes flatpak apenas para conveniência dos utilizadores, não sendo responsável pelo funcionamento dos próprios pacotes flatpak. Para mais informação (em inglês) consultar &apos;flatpaks&apos; no Wiki.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="967"/>
        <location filename="../mainwindow.cpp" line="2928"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="971"/>
        <source>Do not show this message again</source>
        <translation>Não mostrar esta mensagem novamente</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1073"/>
        <source>Remove</source>
        <translation>Remover</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1087"/>
        <source>The following packages were selected. Click Show Details for list of changes.</source>
        <translation>Foram selecionados os seguintes pacotes. Clique em Mostrar detalhes para obter a lista de alterações.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1122"/>
        <location filename="../mainwindow.cpp" line="1251"/>
        <location filename="../mainwindow.cpp" line="1455"/>
        <source>Internet is not available, won&apos;t be able to download the list of packages</source>
        <translation>Sem ligação à Internet; é possível descarregar a lista de pacotes</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1127"/>
        <source>Installing packages...</source>
        <translation>A instalar pacotes...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1184"/>
        <source>Post-processing...</source>
        <translation>Pós-processamento...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1214"/>
        <source>Pre-processing for </source>
        <translation>Pré-processando para </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1227"/>
        <source>Installing </source>
        <translation>A instalar </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1234"/>
        <source>Post-processing for </source>
        <translation>Pós-processando para </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1393"/>
        <source>There was an error writing file: %1. Please check if you have enough free space on your drive</source>
        <translation>Houve um erro ao gravar o ficheiro: %1. Verifique se tem espaço livre suficiente na sua unidade</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1463"/>
        <source>Downloading package info...</source>
        <translation>A descarregar informação do pacote...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1919"/>
        <location filename="../mainwindow.cpp" line="1996"/>
        <location filename="../mainwindow.cpp" line="2020"/>
        <source>Package info</source>
        <translation>Informação do pacote</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1933"/>
        <location filename="../mainwindow.cpp" line="2861"/>
        <source>More &amp;info...</source>
        <translation>Mais &amp;informação...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1962"/>
        <source>Packages to be installed: </source>
        <translation>Pacotes a instalar: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2128"/>
        <location filename="../mainwindow.cpp" line="2141"/>
        <location filename="../mainwindow.cpp" line="2156"/>
        <location filename="../mainwindow.cpp" line="2171"/>
        <location filename="../mainwindow.cpp" line="2260"/>
        <location filename="../mainwindow.cpp" line="2283"/>
        <location filename="../mainwindow.cpp" line="2715"/>
        <location filename="../mainwindow.cpp" line="2789"/>
        <location filename="../mainwindow.cpp" line="2819"/>
        <location filename="../mainwindow.cpp" line="2912"/>
        <source>Done</source>
        <translation>Concluído</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2128"/>
        <location filename="../mainwindow.cpp" line="2141"/>
        <location filename="../mainwindow.cpp" line="2156"/>
        <location filename="../mainwindow.cpp" line="2171"/>
        <location filename="../mainwindow.cpp" line="2260"/>
        <location filename="../mainwindow.cpp" line="2283"/>
        <location filename="../mainwindow.cpp" line="2301"/>
        <location filename="../mainwindow.cpp" line="2715"/>
        <location filename="../mainwindow.cpp" line="2789"/>
        <location filename="../mainwindow.cpp" line="2819"/>
        <location filename="../mainwindow.cpp" line="2912"/>
        <location filename="../mainwindow.cpp" line="2936"/>
        <source>Processing finished successfully.</source>
        <translation>Processamento terminado com êxito.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2133"/>
        <location filename="../mainwindow.cpp" line="2163"/>
        <location filename="../mainwindow.cpp" line="2175"/>
        <location filename="../mainwindow.cpp" line="2720"/>
        <location filename="../mainwindow.cpp" line="2796"/>
        <location filename="../mainwindow.cpp" line="2826"/>
        <source>Problem detected while installing, please inspect the console output.</source>
        <translation>Detectado problema ao instalar; ver informação no separador de processamento.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2185"/>
        <source>About %1</source>
        <translation>Sobre o %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2186"/>
        <source>Version: </source>
        <translation>Versão: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2188"/>
        <source>Package Installer for MX Linux</source>
        <translation>Gestor de Programas/Pacotes do MX Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2190"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Direitos de autor (c) MX Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2191"/>
        <source>%1 License</source>
        <translation>Licença do %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2204"/>
        <source>%1 Help</source>
        <translation>Ajuda do %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2288"/>
        <source>We encountered a problem uninstalling, please check output</source>
        <translation>Foi encontrado um problema ao desinstalar. Ver no separador de processamento</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2301"/>
        <location filename="../mainwindow.cpp" line="2936"/>
        <source>Success</source>
        <translation>Êxito</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2307"/>
        <location filename="../mainwindow.cpp" line="2942"/>
        <source>We encountered a problem uninstalling the program</source>
        <translation>Houve um problema ao desinstalar o programa</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2365"/>
        <location filename="../mainwindow.cpp" line="2382"/>
        <location filename="../mainwindow.cpp" line="2399"/>
        <source>Could not download the list of packages. Please check your APT sources.</source>
        <translation>Erro ao descarregar lista de pacotes. Verifique as Sources do APT</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2415"/>
        <location filename="../mainwindow.cpp" line="2427"/>
        <source>Flatpak not installed</source>
        <translation>Pacote flatpak não instalado</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2416"/>
        <source>Flatpak is not currently installed.
OK to go ahead and install it?</source>
        <translation>O pacote flatpak não está instalado.
Continuar e instalar o pacote?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2427"/>
        <source>Flatpak was not installed</source>
        <translation>O pacote flatpak não estava instalado</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2451"/>
        <location filename="../mainwindow.cpp" line="2473"/>
        <source>Flathub remote failed</source>
        <translation>Falha no repositório remoto Flathub</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2451"/>
        <location filename="../mainwindow.cpp" line="2473"/>
        <source>Flathub remote could not be added</source>
        <translation>Não foi possível adicionar o repositório remoto Flathub</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2459"/>
        <source>Needs re-login</source>
        <translation>É necessário re-entrar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2460"/>
        <source>You might need to logout/login to see installed items in the menu</source>
        <translation>Poderá ser necessário sair e re-entrar para que os itens fiquem visíveis no menu</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2636"/>
        <source>Upgrade</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2748"/>
        <source>Quit?</source>
        <translation>Sair?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2749"/>
        <source>Process still running, quitting might leave the system in an unstable state.&lt;p&gt;&lt;b&gt;Are you sure you want to exit MX Package Installer?&lt;/b&gt;</source>
        <translation>Processo ainda em execução. Sair pode deixar o sistema num estado instável.&lt;p&gt;&lt;b&gt;Sair realmente do MX Instalador de Pacotes?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2893"/>
        <source>Reinstall</source>
        <translation>Reinstalar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2919"/>
        <source>Problem detected during last operation, please inspect the console output.</source>
        <translation>Problema detectado durante a última operação. Inspecione o resultado da consola.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2929"/>
        <source>Potentially dangerous operation.
Please make sure you check carefully the list of packages to be removed.</source>
        <translation>Operação potencialmente perigosa.
Verifique cuidadosamente a lista de pacotes a serem removidos.</translation>
    </message>
</context>
<context>
    <name>ManageRemotes</name>
    <message>
        <location filename="../remotes.cpp" line="13"/>
        <source>Manage Flatpak Remotes</source>
        <translation>Gerir repositórios remotos flatpak</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="22"/>
        <source>For all users</source>
        <translation>Para todos os utilizadores</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="23"/>
        <source>For current user</source>
        <translation>Para o presente utilizador</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="29"/>
        <source>enter Flatpak remote URL</source>
        <translation>introduzir a URL do repositório remoto flatpak</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="32"/>
        <source>enter Flatpakref location to install app</source>
        <translation>para instalar uma aplicação, introduzir a localização do respectivo ficheiro .flatpakref</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="34"/>
        <source>Add or remove flatpak remotes (repos), or install apps using flatpakref URL or path</source>
        <translation>Adicionar ou remover repositórios remotos flatpak ou instalar aplicações usando a URL ou o caminho para o respectivo ficheiro .flatpakref</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="43"/>
        <source>Remove remote</source>
        <translation>Remover repositório remoto</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="48"/>
        <source>Add remote</source>
        <translation>Adicionar repositório remoto</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="53"/>
        <source>Install app</source>
        <translation>Instalar aplicação</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="58"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="75"/>
        <source>Not removable</source>
        <translation>Não removível</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="76"/>
        <source>Flathub is the main Flatpak remote and won&apos;t be removed</source>
        <translation>O Flathub é o principal repositório remoto flatpak e não será removido</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="96"/>
        <source>Error adding remote</source>
        <translation>Erro ao adicionar o repositório remoto</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="97"/>
        <source>Could not add remote - command returned an error. Please double-check the remote address and try again</source>
        <translation>Não foi possível adicionar o repositório remoto - o comando resultou num erro. Verificar o endereço do repositório remoto e voltar a tentar</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="102"/>
        <source>Success</source>
        <translation>Êxito</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="102"/>
        <source>Remote added successfully</source>
        <translation>Repositório remoto adicionado com êxito</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../about.cpp" line="52"/>
        <source>License</source>
        <translation>Licença</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="53"/>
        <location filename="../about.cpp" line="62"/>
        <source>Changelog</source>
        <translation>Registo de alterações</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="54"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>&amp;Fechar</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>MX Package Installer is a tool used for managing packages on MX Linux
    - installs popular programs from different sources
    - installs programs from the MX Test repo
    - installs programs from Debian Backports repo
    - installs flatpaks</source>
        <translation>O Instalador de Pacotes do MX Package gere os pacotes de software do MX Linux
    - instala programas populares, de diferentes fontes
    - instala programas do repositório de testes do MX
    - instala programas do repositório Debian Backports
    - instala flatpaks</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="82"/>
        <source>Skip online check if it falsely reports lack of internet access.</source>
        <translation>Ignorar verificação de estado on-line se, por lapso, reporta falta de acesso à Internet</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="88"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="89"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>O utilizador parece ser o root; para usar este programa, sair e voltar a entrar como utilizador normal.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="99"/>
        <source>Unable to get exclusive lock</source>
        <translation>Não foi possível obter o bloqueio exclusivo</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="100"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>Outra aplicação de gestão de pacotes (como o Synaptic ou apt-get), já está em execução. Feche essa aplicação primeiro</translation>
    </message>
</context>
</TS>
