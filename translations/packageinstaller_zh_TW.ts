<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <location filename="../mainwindow.cpp" line="100"/>
        <source>MX Package Installer</source>
        <translation>MX 軟體安裝工具</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="57"/>
        <source>Popular Applications</source>
        <translation>熱門程式</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="73"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="83"/>
        <source>Package</source>
        <translation>軟體</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="88"/>
        <source>Info</source>
        <translation>資料</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="93"/>
        <location filename="../mainwindow.ui" line="468"/>
        <location filename="../mainwindow.ui" line="660"/>
        <location filename="../mainwindow.ui" line="1030"/>
        <source>Description</source>
        <translation>簡介</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="101"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Manage popular packages&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="120"/>
        <location filename="../mainwindow.ui" line="191"/>
        <location filename="../mainwindow.ui" line="730"/>
        <location filename="../mainwindow.ui" line="1055"/>
        <location filename="../mainwindow.ui" line="1274"/>
        <source>search</source>
        <translation>搜尋</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="149"/>
        <location filename="../mainwindow.ui" line="204"/>
        <location filename="../mainwindow.ui" line="772"/>
        <location filename="../mainwindow.ui" line="1118"/>
        <location filename="../mainwindow.ui" line="1245"/>
        <source>= Installed packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="173"/>
        <source>Enabled Repos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="237"/>
        <source>Remove orphan packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="264"/>
        <location filename="../mainwindow.ui" line="1337"/>
        <source>Upgrade All</source>
        <translation>全部升級</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="278"/>
        <location filename="../mainwindow.ui" line="558"/>
        <location filename="../mainwindow.ui" line="886"/>
        <source>Installed:</source>
        <translation>已安裝：</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="301"/>
        <location filename="../mainwindow.ui" line="508"/>
        <location filename="../mainwindow.ui" line="907"/>
        <source>Total packages:</source>
        <translation>軟體總數：</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="315"/>
        <location filename="../mainwindow.ui" line="615"/>
        <location filename="../mainwindow.ui" line="947"/>
        <source>Also Install &quot;Recommended&quot; Packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="322"/>
        <location filename="../mainwindow.ui" line="588"/>
        <location filename="../mainwindow.ui" line="879"/>
        <source>Upgradable:</source>
        <translation>可升級：</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="329"/>
        <location filename="../mainwindow.ui" line="522"/>
        <location filename="../mainwindow.ui" line="914"/>
        <source>Hide library and developer packages</source>
        <translation>隱藏函式庫和軟體研發程式</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="351"/>
        <location filename="../mainwindow.ui" line="544"/>
        <location filename="../mainwindow.ui" line="966"/>
        <source>Refresh list</source>
        <translation>重新載入清單</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="390"/>
        <location filename="../mainwindow.ui" line="688"/>
        <location filename="../mainwindow.ui" line="1085"/>
        <location filename="../mainwindow.ui" line="1197"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filter packages according to their status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;依軟體狀態篩選。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="393"/>
        <location filename="../mainwindow.ui" line="397"/>
        <location filename="../mainwindow.ui" line="691"/>
        <location filename="../mainwindow.ui" line="695"/>
        <location filename="../mainwindow.ui" line="1088"/>
        <location filename="../mainwindow.ui" line="1092"/>
        <location filename="../mainwindow.cpp" line="2541"/>
        <source>All packages</source>
        <translation>所有軟體</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="402"/>
        <location filename="../mainwindow.ui" line="700"/>
        <location filename="../mainwindow.ui" line="1097"/>
        <location filename="../mainwindow.cpp" line="2554"/>
        <source>Installed</source>
        <translation>已安裝</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <location filename="../mainwindow.ui" line="705"/>
        <location filename="../mainwindow.ui" line="1102"/>
        <location filename="../mainwindow.cpp" line="2552"/>
        <source>Upgradable</source>
        <translation>可升級</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="412"/>
        <location filename="../mainwindow.ui" line="710"/>
        <location filename="../mainwindow.ui" line="1107"/>
        <location filename="../mainwindow.ui" line="1234"/>
        <location filename="../mainwindow.cpp" line="2526"/>
        <location filename="../mainwindow.cpp" line="2557"/>
        <location filename="../mainwindow.cpp" line="2649"/>
        <location filename="../mainwindow.cpp" line="2650"/>
        <source>Not installed</source>
        <translation>未安裝</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="432"/>
        <location filename="../mainwindow.ui" line="752"/>
        <location filename="../mainwindow.ui" line="814"/>
        <source>= Upgradable package. Newer version available in selected repository.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="458"/>
        <location filename="../mainwindow.ui" line="650"/>
        <location filename="../mainwindow.ui" line="1020"/>
        <source>Package Name</source>
        <translation>軟體名稱</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="463"/>
        <location filename="../mainwindow.ui" line="655"/>
        <location filename="../mainwindow.ui" line="1025"/>
        <location filename="../mainwindow.ui" line="1469"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="493"/>
        <source>MX Test Repo</source>
        <translation>MX 測試倉庫</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="796"/>
        <source>Debian Backports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1142"/>
        <source>Flatpaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1148"/>
        <location filename="../mainwindow.ui" line="1152"/>
        <source>For all users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1157"/>
        <source>For current user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1165"/>
        <source>Remote (repo):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1200"/>
        <location filename="../mainwindow.ui" line="1204"/>
        <location filename="../mainwindow.cpp" line="2508"/>
        <source>All apps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1209"/>
        <location filename="../mainwindow.cpp" line="2512"/>
        <source>All runtimes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1214"/>
        <location filename="../mainwindow.cpp" line="2516"/>
        <source>All available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1219"/>
        <location filename="../mainwindow.cpp" line="2506"/>
        <source>Installed apps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1224"/>
        <location filename="../mainwindow.cpp" line="2504"/>
        <source>Installed runtimes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1229"/>
        <location filename="../mainwindow.cpp" line="2524"/>
        <location filename="../mainwindow.cpp" line="2643"/>
        <location filename="../mainwindow.cpp" line="2644"/>
        <source>All installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1316"/>
        <source>Total items </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1330"/>
        <source>Installed apps:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1383"/>
        <source>Advanced</source>
        <translation>進階選項</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1413"/>
        <source>Total installed size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1427"/>
        <source>Remove unused runtimes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1459"/>
        <source>Short Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1464"/>
        <source>Full Package Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1474"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1504"/>
        <location filename="../mainwindow.cpp" line="2315"/>
        <location filename="../mainwindow.cpp" line="2464"/>
        <source>Console Output</source>
        <translation>終端機輸出</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1510"/>
        <source>Enter</source>
        <translation>Enter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1521"/>
        <source>Respond here, or just press Enter</source>
        <translation>在此回覆，或只按 Enter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1577"/>
        <location filename="../mainwindow.cpp" line="1075"/>
        <location filename="../mainwindow.cpp" line="2636"/>
        <location filename="../mainwindow.cpp" line="2640"/>
        <location filename="../mainwindow.cpp" line="2895"/>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1584"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1603"/>
        <source>Quit application</source>
        <translation>退出程式</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1606"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1613"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1645"/>
        <source>About this application</source>
        <translation>關於本程式</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1648"/>
        <source>About...</source>
        <translation>關於……</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1655"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1693"/>
        <source>Display help </source>
        <translation>顯示說明</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1696"/>
        <source>Help</source>
        <translation>說明</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1703"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1719"/>
        <source>Uninstall</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1726"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="91"/>
        <source>Flatpak tab is disabled on 32-bit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="184"/>
        <source>Uninstalling packages...</source>
        <translation>正在移除軟體……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="189"/>
        <source>Running pre-uninstall operations...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="204"/>
        <source>Running post-uninstall operations...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="218"/>
        <source>Refreshing sources...</source>
        <translation>更新來源……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="232"/>
        <location filename="../mainwindow.cpp" line="1121"/>
        <location filename="../mainwindow.cpp" line="1250"/>
        <location filename="../mainwindow.cpp" line="1392"/>
        <location filename="../mainwindow.cpp" line="1454"/>
        <location filename="../mainwindow.cpp" line="2132"/>
        <location filename="../mainwindow.cpp" line="2162"/>
        <location filename="../mainwindow.cpp" line="2174"/>
        <location filename="../mainwindow.cpp" line="2288"/>
        <location filename="../mainwindow.cpp" line="2307"/>
        <location filename="../mainwindow.cpp" line="2364"/>
        <location filename="../mainwindow.cpp" line="2381"/>
        <location filename="../mainwindow.cpp" line="2398"/>
        <location filename="../mainwindow.cpp" line="2719"/>
        <location filename="../mainwindow.cpp" line="2795"/>
        <location filename="../mainwindow.cpp" line="2825"/>
        <location filename="../mainwindow.cpp" line="2918"/>
        <location filename="../mainwindow.cpp" line="2942"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="233"/>
        <source>There was a problem updating sources. Some sources may not have provided updates. For more info check: </source>
        <translation>更新來源時發生問題。可能有一些軟體來源並未提供更新檔。詳細訊息請見：</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="625"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="632"/>
        <source>Please wait...</source>
        <translation>請稍待……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="814"/>
        <location filename="../mainwindow.cpp" line="831"/>
        <source>Version </source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="814"/>
        <source> in stable repo</source>
        <translation>在安定倉庫裡面</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="816"/>
        <source>Not available in stable repo</source>
        <translation>安定倉庫裡面沒有該軟體</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="825"/>
        <location filename="../mainwindow.cpp" line="2441"/>
        <source>Latest version </source>
        <translation>最新版本</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="825"/>
        <location filename="../mainwindow.cpp" line="2441"/>
        <source> already installed</source>
        <translation>在系統上已經安裝了</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="831"/>
        <source> installed</source>
        <translation>已安裝</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="943"/>
        <source>You are about to use the MX Test repository, whose packages are provided for testing purposes only. It is possible that they might break your system, so it is suggested that you back up your system and install or update only one package at a time. Please provide feedback in the Forum so the package can be evaluated before moving up to Main.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="952"/>
        <source>You are about to use Debian Backports, which contains packages taken from the next Debian release (called &apos;testing&apos;), adjusted and recompiled for usage on Debian stable. They cannot be tested as extensively as in the stable releases of Debian and MX Linux, and are provided on an as-is basis, with risk of incompatibilities with other components in Debian stable. Use with care!</source>
        <translation>即將利用 Debian 回溯移植倉庫，裡面的軟體來自 Debian 下一個正式發行版（稱為 &apos;testing&apos; ），經過調校、編譯，以供 Debian 安定版使用。這些軟體的測試過程沒辦法像 Debian 和 MX Linux 的安定版那麼精細，基本上它們以依其所是的狀態提供出來，有可能與 Debian 安定版當中的其他元件並不相容。小心使用！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="960"/>
        <source>MX Linux includes this repository of flatpaks for the users&apos; convenience only, and is not responsible for the functionality of the individual flatpaks themselves. For more, consult flatpaks in the Wiki.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="967"/>
        <location filename="../mainwindow.cpp" line="2928"/>
        <source>Warning</source>
        <translation>注意</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="971"/>
        <source>Do not show this message again</source>
        <translation>不再顯示此一訊息</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1073"/>
        <source>Remove</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1087"/>
        <source>The following packages were selected. Click Show Details for list of changes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1122"/>
        <location filename="../mainwindow.cpp" line="1251"/>
        <location filename="../mainwindow.cpp" line="1455"/>
        <source>Internet is not available, won&apos;t be able to download the list of packages</source>
        <translation>無法連上網際網路，因此不能下載軟體列表。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1127"/>
        <source>Installing packages...</source>
        <translation>正在安裝軟體……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1184"/>
        <source>Post-processing...</source>
        <translation>後續處理當中……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1214"/>
        <source>Pre-processing for </source>
        <translation>前置處理</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1227"/>
        <source>Installing </source>
        <translation>正在安裝……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1234"/>
        <source>Post-processing for </source>
        <translation>後續處理</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1393"/>
        <source>There was an error writing file: %1. Please check if you have enough free space on your drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1463"/>
        <source>Downloading package info...</source>
        <translation>正在下載軟體資料……</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1919"/>
        <location filename="../mainwindow.cpp" line="1996"/>
        <location filename="../mainwindow.cpp" line="2020"/>
        <source>Package info</source>
        <translation>軟體資料</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1933"/>
        <location filename="../mainwindow.cpp" line="2861"/>
        <source>More &amp;info...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1962"/>
        <source>Packages to be installed: </source>
        <translation>即將安裝的軟體：</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2128"/>
        <location filename="../mainwindow.cpp" line="2141"/>
        <location filename="../mainwindow.cpp" line="2156"/>
        <location filename="../mainwindow.cpp" line="2171"/>
        <location filename="../mainwindow.cpp" line="2260"/>
        <location filename="../mainwindow.cpp" line="2283"/>
        <location filename="../mainwindow.cpp" line="2715"/>
        <location filename="../mainwindow.cpp" line="2789"/>
        <location filename="../mainwindow.cpp" line="2819"/>
        <location filename="../mainwindow.cpp" line="2912"/>
        <source>Done</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2128"/>
        <location filename="../mainwindow.cpp" line="2141"/>
        <location filename="../mainwindow.cpp" line="2156"/>
        <location filename="../mainwindow.cpp" line="2171"/>
        <location filename="../mainwindow.cpp" line="2260"/>
        <location filename="../mainwindow.cpp" line="2283"/>
        <location filename="../mainwindow.cpp" line="2301"/>
        <location filename="../mainwindow.cpp" line="2715"/>
        <location filename="../mainwindow.cpp" line="2789"/>
        <location filename="../mainwindow.cpp" line="2819"/>
        <location filename="../mainwindow.cpp" line="2912"/>
        <location filename="../mainwindow.cpp" line="2936"/>
        <source>Processing finished successfully.</source>
        <translation>程序順利進行完畢。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2133"/>
        <location filename="../mainwindow.cpp" line="2163"/>
        <location filename="../mainwindow.cpp" line="2175"/>
        <location filename="../mainwindow.cpp" line="2720"/>
        <location filename="../mainwindow.cpp" line="2796"/>
        <location filename="../mainwindow.cpp" line="2826"/>
        <source>Problem detected while installing, please inspect the console output.</source>
        <translation>安裝軟體時發現問題，請參考終端機輸出的訊息。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2185"/>
        <source>About %1</source>
        <translation>大約 %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2186"/>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2188"/>
        <source>Package Installer for MX Linux</source>
        <translation>MX Linux 軟體安裝工具</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2190"/>
        <source>Copyright (c) MX Linux</source>
        <translation>版權所有 (c) MX Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2191"/>
        <source>%1 License</source>
        <translation>%1 許可</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2204"/>
        <source>%1 Help</source>
        <translation>%1 幫助</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2288"/>
        <source>We encountered a problem uninstalling, please check output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2301"/>
        <location filename="../mainwindow.cpp" line="2936"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2307"/>
        <location filename="../mainwindow.cpp" line="2942"/>
        <source>We encountered a problem uninstalling the program</source>
        <translation>移除該程式時發生問題</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2365"/>
        <location filename="../mainwindow.cpp" line="2382"/>
        <location filename="../mainwindow.cpp" line="2399"/>
        <source>Could not download the list of packages. Please check your APT sources.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2415"/>
        <location filename="../mainwindow.cpp" line="2427"/>
        <source>Flatpak not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2416"/>
        <source>Flatpak is not currently installed.
OK to go ahead and install it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2427"/>
        <source>Flatpak was not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2451"/>
        <location filename="../mainwindow.cpp" line="2473"/>
        <source>Flathub remote failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2451"/>
        <location filename="../mainwindow.cpp" line="2473"/>
        <source>Flathub remote could not be added</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2459"/>
        <source>Needs re-login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2460"/>
        <source>You might need to logout/login to see installed items in the menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2636"/>
        <source>Upgrade</source>
        <translation>升級</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2748"/>
        <source>Quit?</source>
        <translation>退出？</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2749"/>
        <source>Process still running, quitting might leave the system in an unstable state.&lt;p&gt;&lt;b&gt;Are you sure you want to exit MX Package Installer?&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2893"/>
        <source>Reinstall</source>
        <translation>重新安裝</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2919"/>
        <source>Problem detected during last operation, please inspect the console output.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2929"/>
        <source>Potentially dangerous operation.
Please make sure you check carefully the list of packages to be removed.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManageRemotes</name>
    <message>
        <location filename="../remotes.cpp" line="13"/>
        <source>Manage Flatpak Remotes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="22"/>
        <source>For all users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="23"/>
        <source>For current user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="29"/>
        <source>enter Flatpak remote URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="32"/>
        <source>enter Flatpakref location to install app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="34"/>
        <source>Add or remove flatpak remotes (repos), or install apps using flatpakref URL or path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="43"/>
        <source>Remove remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="48"/>
        <source>Add remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="53"/>
        <source>Install app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="58"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="75"/>
        <source>Not removable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="76"/>
        <source>Flathub is the main Flatpak remote and won&apos;t be removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="96"/>
        <source>Error adding remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="97"/>
        <source>Could not add remote - command returned an error. Please double-check the remote address and try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="102"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../remotes.cpp" line="102"/>
        <source>Remote added successfully</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../about.cpp" line="52"/>
        <source>License</source>
        <translation>授權條款</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="53"/>
        <location filename="../about.cpp" line="62"/>
        <source>Changelog</source>
        <translation>變更紀錄</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="54"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>關閉（&amp;C）</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>MX Package Installer is a tool used for managing packages on MX Linux
    - installs popular programs from different sources
    - installs programs from the MX Test repo
    - installs programs from Debian Backports repo
    - installs flatpaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="82"/>
        <source>Skip online check if it falsely reports lack of internet access.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="88"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="89"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="99"/>
        <source>Unable to get exclusive lock</source>
        <translation>無法獨佔鎖定</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="100"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>系統上已經有別的軟體套件管理程式（例如 Synaptic 或 apt-get）在運作，請先關閉該程式。</translation>
    </message>
</context>
</TS>
