# **********************************************************************
# * Copyright (C) 2017 MX Authors
# *
# * Authors: Adrian
# *          Dolphin_Oracle
# *          MX Linux <http://mxlinux.org>
# *
# * This file is part of packageinstaller.
# *
# * packageinstaller is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * packageinstaller is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with packageinstaller.  If not, see <http://www.gnu.org/licenses/>.
# **********************************************************************/

QT       += core gui xml network widgets
CONFIG   += c++1z

TARGET = packageinstaller
TEMPLATE = app

# The following define makes your compiler warn you if you use any
# feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
    mainwindow.cpp \
    lockfile.cpp \
    versionnumber.cpp \
    aptcache.cpp \
    remotes.cpp \
    about.cpp \
    cmd.cpp

HEADERS  += \
    mainwindow.h \
    lockfile.h \
    versionnumber.h \
    aptcache.h \
    remotes.h \
    version.h \
    about.h \
    cmd.h

FORMS    += \
    mainwindow.ui

TRANSLATIONS += translations/packageinstaller_af.ts \
                translations/packageinstaller_am.ts \
                translations/packageinstaller_ar.ts \
                translations/packageinstaller_be.ts \
                translations/packageinstaller_bg.ts \
                translations/packageinstaller_bn.ts \
                translations/packageinstaller_bs_BA.ts \
                translations/packageinstaller_bs.ts \
                translations/packageinstaller_ca.ts \
                translations/packageinstaller_ceb.ts \
                translations/packageinstaller_co.ts \
                translations/packageinstaller_cs.ts \
                translations/packageinstaller_cy.ts \
                translations/packageinstaller_da.ts \
                translations/packageinstaller_de.ts \
                translations/packageinstaller_el.ts \
                translations/packageinstaller_en_GB.ts \
                translations/packageinstaller_en.ts \
                translations/packageinstaller_en_US.ts \
                translations/packageinstaller_eo.ts \
                translations/packageinstaller_es_ES.ts \
                translations/packageinstaller_es.ts \
                translations/packageinstaller_et.ts \
                translations/packageinstaller_eu.ts \
                translations/packageinstaller_fa.ts \
                translations/packageinstaller_fi_FI.ts \
                translations/packageinstaller_fil_PH.ts \
                translations/packageinstaller_fil.ts \
                translations/packageinstaller_fi.ts \
                translations/packageinstaller_fr_BE.ts \
                translations/packageinstaller_fr_FR.ts \
                translations/packageinstaller_fr.ts \
                translations/packageinstaller_fy.ts \
                translations/packageinstaller_ga.ts \
                translations/packageinstaller_gd.ts \
                translations/packageinstaller_gl_ES.ts \
                translations/packageinstaller_gl.ts \
                translations/packageinstaller_gu.ts \
                translations/packageinstaller_ha.ts \
                translations/packageinstaller_haw.ts \
                translations/packageinstaller_he_IL.ts \
                translations/packageinstaller_he.ts \
                translations/packageinstaller_hi.ts \
                translations/packageinstaller_hr.ts \
                translations/packageinstaller_ht.ts \
                translations/packageinstaller_hu.ts \
                translations/packageinstaller_hy.ts \
                translations/packageinstaller_id.ts \
                translations/packageinstaller_is.ts \
                translations/packageinstaller_it.ts \
                translations/packageinstaller_ja.ts \
                translations/packageinstaller_jv.ts \
                translations/packageinstaller_ka.ts \
                translations/packageinstaller_kk.ts \
                translations/packageinstaller_km.ts \
                translations/packageinstaller_kn.ts \
                translations/packageinstaller_ko.ts \
                translations/packageinstaller_ku.ts \
                translations/packageinstaller_ky.ts \
                translations/packageinstaller_lb.ts \
                translations/packageinstaller_lo.ts \
                translations/packageinstaller_lt.ts \
                translations/packageinstaller_lv.ts \
                translations/packageinstaller_mg.ts \
                translations/packageinstaller_mi.ts \
                translations/packageinstaller_mk.ts \
                translations/packageinstaller_ml.ts \
                translations/packageinstaller_mn.ts \
                translations/packageinstaller_mr.ts \
                translations/packageinstaller_ms.ts \
                translations/packageinstaller_mt.ts \
                translations/packageinstaller_my.ts \
                translations/packageinstaller_nb_NO.ts \
                translations/packageinstaller_nb.ts \
                translations/packageinstaller_ne.ts \
                translations/packageinstaller_nl_BE.ts \
                translations/packageinstaller_nl.ts \
                translations/packageinstaller_ny.ts \
                translations/packageinstaller_or.ts \
                translations/packageinstaller_pa.ts \
                translations/packageinstaller_pl.ts \
                translations/packageinstaller_ps.ts \
                translations/packageinstaller_pt_BR.ts \
                translations/packageinstaller_pt.ts \
                translations/packageinstaller_ro.ts \
                translations/packageinstaller_rue.ts \
                translations/packageinstaller_ru_RU.ts \
                translations/packageinstaller_ru.ts \
                translations/packageinstaller_rw.ts \
                translations/packageinstaller_sd.ts \
                translations/packageinstaller_si.ts \
                translations/packageinstaller_sk.ts \
                translations/packageinstaller_sl.ts \
                translations/packageinstaller_sm.ts \
                translations/packageinstaller_sn.ts \
                translations/packageinstaller_so.ts \
                translations/packageinstaller_sq.ts \
                translations/packageinstaller_sr.ts \
                translations/packageinstaller_st.ts \
                translations/packageinstaller_su.ts \
                translations/packageinstaller_sv.ts \
                translations/packageinstaller_sw.ts \
                translations/packageinstaller_ta.ts \
                translations/packageinstaller_te.ts \
                translations/packageinstaller_tg.ts \
                translations/packageinstaller_th.ts \
                translations/packageinstaller_tk.ts \
                translations/packageinstaller_tr.ts \
                translations/packageinstaller_tt.ts \
                translations/packageinstaller_ug.ts \
                translations/packageinstaller_uk.ts \
                translations/packageinstaller_ur.ts \
                translations/packageinstaller_uz.ts \
                translations/packageinstaller_vi.ts \
                translations/packageinstaller_xh.ts \
                translations/packageinstaller_yi.ts \
                translations/packageinstaller_yo.ts \
                translations/packageinstaller_yue_CN.ts \
                translations/packageinstaller_zh_CN.ts \
                translations/packageinstaller_zh_HK.ts \
                translations/packageinstaller_zh_TW.ts

RESOURCES += \
    images.qrc

DISTFILES += \
    icons/package-installed-outdated.png \
    icons/package-installed-updated.png
